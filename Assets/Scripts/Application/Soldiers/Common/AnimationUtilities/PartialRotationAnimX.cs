﻿using System;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Common.AnimationUtilities
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    internal class PartialRotationAnimX : MonoBehaviour
    {
        private Transform _transform;
        private Transform Transform => _transform ??= transform;

        [HideInInspector] public float rotation;

        private void Update()
        {
            UpdateRotation();
        }

        private void UpdateRotation()
        {
#if UNITY_EDITOR
            _transform = transform;
#endif
            var euler = Transform.eulerAngles;
            euler.x = rotation;
            Transform.eulerAngles = euler;
        }
    }
}