﻿using System;
using System.Collections.Generic;
using System.Linq;
using TowerDefence.Application.Soldiers.Config;
using TowerDefence.Application.Soldiers.Logic.SoldierLogic;
using TowerDefence.Core.GameAssetBase;
using TowerDefence.Enterprise.AbstractEntities;
using TowerDefence.Enterprise.Entities;
using UnityEngine;
using Zenject;

namespace TowerDefence.Application.Soldiers.Services
{
    internal class SoldierService : MonoBehaviour, ISoldierService
    {
        [SerializeField] private BezierCurve roadPath;
        [SerializeField] private PlayerSoldierConfig playerSoldierConfig;
        [SerializeField] private EnemySoldierConfig enemySoldierConfig;
        private List<MonoEnemySoldier> _currentEnemySoldiers = new();
        private List<MonoPlayerSoldier> _currentPlayerSoldiers = new();
        public IEnumerable<IMonoSoldier> PlayerPrefabs => playerSoldierConfig.Soldiers;
        public IEnumerable<IMonoSoldier> EnemyPrefabs => enemySoldierConfig.Soldiers;
        public IEnumerable<IMonoSoldier> CurrentEnemySoldiers => _currentEnemySoldiers;
        public IEnumerable<IMonoSoldier> CurrentPlayerSoldiers => _currentPlayerSoldiers;
        public event ISoldierService.SoldierDelegate OnSoldierSpawned;
        public event ISoldierService.SoldierDelegate OnSoldierHpChanged;
        public event ISoldierService.SoldierDelegate OnSoldierDestroyed;
        public event ISoldierService.SoldierDelegate OnSoldierDied;
        public event ISoldierService.EnemySoldierDelegate OnEnemyReachedEndPoint;

        public IMonoSoldier ClonePlayerSoldier(string prefabId, PlayerSoldier initialData = null)
        {
            var prefab = playerSoldierConfig.Soldiers.First(s => s.Data.prefabId == prefabId);
            var instance = prefab.Instantiate() as MonoPlayerSoldier;
            instance.Initialize(initialData ?? instance.Data);
            _currentPlayerSoldiers.Add(instance);
            OnSoldierSpawned?.Invoke(instance);
            SubscribeSoldier(instance);
            instance.OnDestroying += delegate(IGameAssetBehaviour<PlayerSoldier> soldier) { OnSoldierDestroyed?.Invoke(soldier as IMonoSoldier); };
            return instance;
        }

        public IMonoSoldier CloneEnemySoldier(string prefabId)
        {
            var prefab = enemySoldierConfig.Soldiers.First(s => s.Data.prefabId == prefabId);
            var instance = prefab.Instantiate() as MonoEnemySoldier;
            instance.RoadPath = roadPath;
            instance.Initialize(instance.Data);
            _currentEnemySoldiers.Add(instance);
            OnSoldierSpawned?.Invoke(instance);
            SubscribeSoldier(instance);
            instance.OnDestroying += delegate(IGameAssetBehaviour<EnemySoldier> soldier) { OnSoldierDestroyed?.Invoke(soldier as IMonoSoldier); };
            instance.OnReachedEndPoint += delegate(IMonoEnemySoldier soldier)
            {
                UnsubscribeSoldier(soldier);
                _currentEnemySoldiers.Remove(soldier as MonoEnemySoldier);
                OnEnemyReachedEndPoint?.Invoke(soldier);
            };
            return instance;
        }

        public void DestroyAllSoldiers()
        {
             var enemiesCount = _currentEnemySoldiers.Count;
             for (int i = 0; i < enemiesCount; i++)
             {
                 _currentEnemySoldiers[i].Destroy();
             }
             _currentEnemySoldiers.Clear();
             var playerSoldiersCount = _currentPlayerSoldiers.Count;
             for (int i = 0; i < playerSoldiersCount; i++)
             {
                 _currentPlayerSoldiers[i].Destroy();
             }
             _currentPlayerSoldiers.Clear();
        }

        private void SubscribeSoldier(IMonoSoldier soldier)
        {
            soldier.OnHpChanged += OnEachSoldierHpChanged;
            soldier.OnDied += OnEachSoldierDied;
        }

        private void UnsubscribeSoldier(IMonoSoldier soldier)
        {
            soldier.OnHpChanged -= OnEachSoldierHpChanged;
            soldier.OnDied -= OnEachSoldierDied;
        }

        private void OnEachSoldierHpChanged(IMonoSoldier soldier)
        {
            OnSoldierHpChanged?.Invoke(soldier);
        }

        private void OnEachSoldierDied(IMonoSoldier soldier)
        {
            if (soldier is MonoEnemySoldier enemySoldier)
            {
                _currentEnemySoldiers.Remove(enemySoldier);
            }
            if (soldier is MonoPlayerSoldier playerSoldier)
            {
                _currentPlayerSoldiers.Remove(playerSoldier);
            }
            UnsubscribeSoldier(soldier);
            OnSoldierDied?.Invoke(soldier);
        }
    }
}