using System.Collections.Generic;
using TowerDefence.Application.Soldiers.Logic.SoldierLogic;
using TowerDefence.Enterprise.AbstractEntities;
using TowerDefence.Enterprise.Entities;

namespace TowerDefence.Application.Soldiers.Services
{
    public interface ISoldierService
    {
        public IEnumerable<IMonoSoldier> PlayerPrefabs { get; }
        public IEnumerable<IMonoSoldier> EnemyPrefabs { get; }
        IEnumerable<IMonoSoldier> CurrentEnemySoldiers { get; }
        IEnumerable<IMonoSoldier> CurrentPlayerSoldiers { get; }
        event SoldierDelegate OnSoldierSpawned;
        event SoldierDelegate OnSoldierHpChanged;
        event SoldierDelegate OnSoldierDestroyed;
        event SoldierDelegate OnSoldierDied;
        event EnemySoldierDelegate OnEnemyReachedEndPoint;
        delegate void SoldierDelegate(IMonoSoldier soldier);
        delegate void EnemySoldierDelegate(IMonoEnemySoldier enemySoldier);
        IMonoSoldier ClonePlayerSoldier(string prefabId, PlayerSoldier initialData = null);
        IMonoSoldier CloneEnemySoldier(string prefabId);
        void DestroyAllSoldiers();
    }
}
