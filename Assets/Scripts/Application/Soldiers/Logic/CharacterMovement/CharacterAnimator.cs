﻿using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.CharacterMovement
{
    [RequireComponent(typeof(Animator))]
    internal class CharacterAnimator : MonoBehaviour
    {
        private const string MoveSpeedParameter = "MoveSpeed";
        private static readonly int MoveSpeedId = Animator.StringToHash(MoveSpeedParameter);
        private const string CharacterStateParameter = "CharacterState";
        private static readonly int CharacterStateId = Animator.StringToHash(CharacterStateParameter);
        private Animator _animator;
        private Animator Animator => _animator ??= GetComponent<Animator>();
        private CharacterAnimState _currentState = CharacterAnimState.Idle;

        public void SetMoveSpeed(float moveSpeed)
        {
            Animator.SetFloat(MoveSpeedId, moveSpeed);
        }


        public void SetState(CharacterAnimState state)
        {
            if (_currentState == state)
            {
                return;
            }
            _currentState = state;
            Animator.SetInteger(CharacterStateId, (int)state);
        }

        public enum CharacterAnimState
        {
            Idle = 0,
            Walking = 1,
            Attacking = 2,
            Dead = 3
        }
    }
}