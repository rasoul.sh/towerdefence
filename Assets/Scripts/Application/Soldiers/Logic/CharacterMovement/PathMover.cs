﻿using System.Collections;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.CharacterMovement
{
    [RequireComponent(typeof(CharacterLookAt))]
    internal class PathMover : MonoBehaviour
    {
        [SerializeField] private BezierCurve roadPath;
        [SerializeField] private float lookAtDistance = 0.02f;
        public float MoveSpeed { private get; set; } = 1f;
        private float _traveledPath;
        private Coroutine _moveRoutine;
        private Transform _transform;
        private Transform Transform => _transform ??= transform;
        private CharacterLookAt _lookAt;
        private CharacterLookAt LookAt => _lookAt ??= GetComponent<CharacterLookAt>();
        public event FinishDelegate OnPathFinished;
        public delegate void FinishDelegate();
        private bool _isMoving;
        private float NormalizedMoveSpeed => CharacterGeneralConfig.NormalizeSpeed(MoveSpeed);


        public BezierCurve RoadPath
        {
            get => roadPath;
            set
            {
                roadPath = value;
                Transform.position = roadPath.GetPointAt(0f);
                LookAt.LookAt(roadPath.GetPointAt(lookAtDistance));
            }
        }

        public void ProceedMovement()
        {
            if (_isMoving)
            {
                Debug.LogError("The character is already moving");
                return;
            }
            if (_traveledPath >= 1f)
            {
                Debug.LogError("Cannot start moving while the character is at the end point");
                return;
            }

            _isMoving = true;
            _moveRoutine = StartCoroutine(TravelingRoutine());
        }

        public void StopMovement()
        {
            if (_isMoving == false)
            {
                Debug.LogError("The character has been stopped already");
                return;
            }
            _isMoving = false;
            if (_moveRoutine != null)
            {
                StopCoroutine(_moveRoutine);
            }
        }

        private IEnumerator TravelingRoutine()
        {
            if (_traveledPath is < 1 and > 0)
            {
                var roadPosition = roadPath.GetPointAt(_traveledPath);
                var lookAtRotation = _lookAt.GetLookAtRotation(roadPosition);
                while (_transform.position != roadPosition)
                {
                    _transform.position = Vector3.MoveTowards(_transform.position,
                        roadPosition, MoveSpeed * Time.deltaTime);
                    _transform.rotation = Quaternion.RotateTowards(_transform.rotation,
                        lookAtRotation,CharacterGeneralConfig.CharacterRotateSpeed * Time.deltaTime);
                    yield return null;
                }
            }
            while (Mathf.Abs(1 - _traveledPath) > 0.01f)
            {
                _traveledPath += NormalizedMoveSpeed * Time.deltaTime;
                if (_traveledPath > 1f)
                {
                    _traveledPath = 1f;
                    break;
                }
                var lookAtPoint = _traveledPath + lookAtDistance;
                var nextPosition = roadPath.GetPointAt(_traveledPath);
                _transform.position = nextPosition;
                if (lookAtPoint < 0.95f)
                {
                    var lookAtRotation = _lookAt.GetLookAtRotation(roadPath.GetPointAt(lookAtPoint));
                    _transform.rotation = Quaternion.RotateTowards(_transform.rotation, lookAtRotation,CharacterGeneralConfig.CharacterRotateSpeed *  Time.deltaTime);   
                }
                yield return null;
            }

            _traveledPath = 1f;
            OnPathFinished?.Invoke();
        }
    }
}