﻿using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.CharacterMovement
{
    internal class CharacterLookAt : MonoBehaviour
    {
        private Transform _transform;
        private Transform Transform => _transform ??= transform;
        
        public void LookAt(Vector3 targetPosition)
        {
            Transform.rotation = GetLookAtRotation(targetPosition);
        }

        public Quaternion GetLookAtRotation(Vector3 targetPosition)
        {
            return Quaternion.LookRotation(Transform.position - targetPosition);
        }
    }
}