﻿namespace TowerDefence.Application.Soldiers.Logic.CharacterMovement
{
    internal static class CharacterGeneralConfig
    {
        private const float DistanceMultiplier = 0.01f;
        public const float CharacterRotateSpeed = 200f;
        public static float NormalizeSpeed(float speed)
        {
            return speed * DistanceMultiplier;
        }
    }
}