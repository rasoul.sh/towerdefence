﻿using System.Collections;
using TowerDefence.Application.Soldiers.Logic.CharacterMovement;
using TowerDefence.Application.Soldiers.Logic.SoldierLogic.AI;
using TowerDefence.Core.GameAssetBase;
using TowerDefence.Enterprise.AbstractEntities;
using TowerDefence.Enterprise.Enums;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.SoldierLogic
{
    [RequireComponent(typeof(CharacterAnimator))]
    internal abstract class MonoSoldier<T> : GameAssetBehaviour<T>, IMonoSoldier where T : Soldier
    {
        public Soldier SoldierData => base.Data;
        public Vector3 Position => transform.position;
        public Quaternion Rotation => transform.rotation;
        public abstract SoldierRoles SoldierRole { get; }
        public GameObject ThisGameObject => gameObject;
        public event IMonoSoldier.SoldierDelegate OnHpChanged;
        public event IMonoSoldier.SoldierDelegate OnDied;
        public void DieImmediate()
        {
            OnDied?.Invoke(this);
            Destroy();
        }

        private CharacterAnimator _characterAnimator;

        private void Start()
        {
            _characterAnimator = GetComponent<CharacterAnimator>();
        }

        public override void Initialize(T initialData)
        {
            base.Initialize(initialData);
            Data.currentHp = Data.maxHp;
        }

        public void Damage(float attack)
        {
            if (SoldierData.currentHp <= 0f)
            {
                Debug.LogError("Cannot damage a dead soldier");
                return;
            }
            SoldierData.currentHp -= attack * Time.deltaTime;
            SoldierData.currentHp = Mathf.Max(SoldierData.currentHp, 0f);
            if (SoldierData.currentHp == 0f)
            {
                StartCoroutine(DieRoutine());
            }
            OnHpChanged?.Invoke(this);
        }

        private IEnumerator DieRoutine()
        {
            OnDied?.Invoke(this);
            _characterAnimator.SetState(CharacterAnimator.CharacterAnimState.Dead);
            GetComponent<Collider>().enabled = false;
            GetComponent<SoldierAI>().enabled = false;
            yield return new WaitForSeconds(5f);
            Destroy();
        }
    }
}