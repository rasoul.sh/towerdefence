﻿using TowerDefence.Application.Soldiers.Logic.CharacterMovement;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Enterprise.Enums;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.SoldierLogic
{
    [RequireComponent(typeof(PathMover))]
    internal class MonoEnemySoldier : MonoSoldier<EnemySoldier>, IMonoEnemySoldier
    {
        public BezierCurve RoadPath { get; set; }
        public override SoldierRoles SoldierRole => SoldierRoles.EnemySoldier;
        private PathMover _pathMover;
        private PathMover PathMover => _pathMover ??= GetComponent<PathMover>();
        public event IMonoEnemySoldier.EnemySoldierDelegate OnReachedEndPoint;

        public override void Initialize(EnemySoldier initialData)
        {
            base.Initialize(initialData);
            gameObject.tag = Enterprise.Rules.TagsAndLayersRules.EnemySoldierTag;
            gameObject.layer = LayerMask.NameToLayer(Enterprise.Rules.TagsAndLayersRules.CharacterLayer);
            PathMover.OnPathFinished += OnPathFinished;
        }

        private void OnPathFinished()
        {
            OnReachedEndPoint?.Invoke(this);
            Destroy();
        }
    }
}