﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.SoldierLogic.AI
{
    [RequireComponent(typeof(IMonoSoldier))]
    internal class SoldierSight : MonoBehaviour
    {
        public string OpponentTag { get; set; }
        public bool IncludeOnTowerSoldiers { get; set; }
        private SphereCollider _rangeCollider;
        public IEnumerable<IMonoSoldier> OpponentsOnSight { get; private set; }
        private Transform _transform;
        private IMonoSoldier _thisSoldier;

        private void Start()
        {
            _transform = transform;
            _thisSoldier = GetComponent<IMonoSoldier>();
        }

        public void UpdateSight()
        {
            var cols = Physics.SphereCastAll(_transform.position, _thisSoldier.SoldierData.attackRange, Vector3.one,
                LayerMask.GetMask(OpponentTag)).Select(h => h.collider);
            var opponents = cols.Where(c => c.CompareTag(OpponentTag)).Select(c => c.GetComponent<IMonoSoldier>());
            if (IncludeOnTowerSoldiers == false)
            {
                opponents = opponents.Where(o => o.SoldierData.isOnTower == false);
            }
            OpponentsOnSight = opponents;
        }
    }
}