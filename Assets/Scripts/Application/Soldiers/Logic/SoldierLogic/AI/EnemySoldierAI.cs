﻿using TowerDefence.Application.Soldiers.Logic.CharacterMovement;
using TowerDefence.Enterprise.Rules;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.SoldierLogic.AI
{
    [RequireComponent(typeof(PathMover))]
    [RequireComponent(typeof(MonoEnemySoldier))]
    internal class EnemySoldierAI : PawnSoldierAI
    {
        private PathMover _pathMover;

        protected override string OpponentTag => TagsAndLayersRules.PlayerSoldierTag;

        protected override void Start()
        {
            base.Start();
            _pathMover = GetComponent<PathMover>();
            _pathMover.RoadPath = (thisSoldier as MonoEnemySoldier).RoadPath;
            _pathMover.MoveSpeed = thisSoldier.SoldierData.moveSpeed;
            _pathMover.OnPathFinished += OnPathFinished;
            StartMoving();
        }

        [ContextMenu("Start Moving")]
        private void StartMoving()
        {
            _pathMover.ProceedMovement();
            characterAnimator.SetState(CharacterAnimator.CharacterAnimState.Walking);
        }
        [ContextMenu("Stop Moving")]
        private void StopMoving()
        {
            _pathMover.StopMovement();
            characterAnimator.SetState(CharacterAnimator.CharacterAnimState.Idle);
        }

        private void OnPathFinished()
        {
            characterAnimator.SetState(CharacterAnimator.CharacterAnimState.Idle);
        }

        protected override void StartAttacking()
        {
            base.StartAttacking();
            StopMoving();
        }

        protected override void StopAttacking()
        {
            base.StopAttacking();
            StartMoving();
        }
    }
}