﻿using System;
using TowerDefence.Application.Soldiers.Logic.CharacterMovement;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.SoldierLogic.AI
{
    [RequireComponent(typeof(CharacterAnimator))]
    [RequireComponent(typeof(CharacterLookAt))]
    [RequireComponent(typeof(SoldierSight))]
    [RequireComponent(typeof(IMonoSoldier))]
    internal abstract class SoldierAI : MonoBehaviour
    {
        protected CharacterAnimator characterAnimator;
        protected Transform characterTransform;
        protected CharacterLookAt characterLookAt;
        protected SoldierSight Sight { get; private set; }
        private float _moveSpeed = 1f;
        protected IMonoSoldier thisSoldier;
        protected abstract string OpponentTag { get; }


        public virtual float MoveSpeed
        {
            get => _moveSpeed;
            set
            {
                _moveSpeed = value;
                characterAnimator.SetMoveSpeed(value);
            }
        }

        protected virtual void Start()
        {
            characterTransform = transform;
            thisSoldier = GetComponent<IMonoSoldier>();
            characterAnimator = GetComponent<CharacterAnimator>();
            characterLookAt = GetComponent<CharacterLookAt>();
            Sight = GetComponent<SoldierSight>();
            Sight.OpponentTag = OpponentTag;
        }

        protected virtual void Update()
        {
            Sight.UpdateSight();
        }
    }
}