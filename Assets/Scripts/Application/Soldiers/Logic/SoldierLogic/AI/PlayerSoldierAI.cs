﻿using System.Collections;
using TowerDefence.Application.Soldiers.Logic.CharacterMovement;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Enterprise.Rules;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.SoldierLogic.AI
{
    internal class PlayerSoldierAI : PawnSoldierAI
    {
        protected override string OpponentTag => TagsAndLayersRules.EnemySoldierTag;
        private Coroutine _backToBaseRoutine;

        protected override void Start()
        {
            base.Start();
        }

        protected override void StartAttacking()
        {
            if (_backToBaseRoutine != null)
            {
                StopCoroutine(_backToBaseRoutine);
            }
            base.StartAttacking();
        }

        protected override void StopAttacking()
        {
            base.StopAttacking();
            if (_backToBaseRoutine != null)
            {
                StopCoroutine(_backToBaseRoutine);
            }
            _backToBaseRoutine = StartCoroutine(BackToBaseRoutine());
        }

        public IEnumerator BackToBaseRoutine()
        {
            var playerSoldierData = thisSoldier.SoldierData as PlayerSoldier;
            characterAnimator.SetState(CharacterAnimator.CharacterAnimState.Walking);
            var baseLookAtRotation = characterLookAt.GetLookAtRotation(playerSoldierData.spawnPosition);
            while (characterTransform.position != playerSoldierData.spawnPosition)
            {
                characterTransform.position = Vector3.MoveTowards(characterTransform.position,
                    playerSoldierData.spawnPosition, MoveSpeed * Time.deltaTime);
                characterTransform.rotation = Quaternion.RotateTowards(characterTransform.rotation,
                    baseLookAtRotation, CharacterGeneralConfig.CharacterRotateSpeed * Time.deltaTime);
                yield return null;
            }
            characterAnimator.SetState(CharacterAnimator.CharacterAnimState.Idle);
        }
    }
}