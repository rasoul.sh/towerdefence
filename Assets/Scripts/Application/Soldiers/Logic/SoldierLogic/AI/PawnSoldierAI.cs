﻿using System.Linq;
using TowerDefence.Application.Soldiers.Logic.CharacterMovement;
using TowerDefence.Enterprise.Rules;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.SoldierLogic.AI
{
    internal abstract class PawnSoldierAI : SoldierAI
    {
        protected IMonoSoldier FightingOpponent { get; set; }

        protected override void Start()
        {
            base.Start();
            MoveSpeed = thisSoldier.SoldierData.moveSpeed;
        }

        protected override void Update()
        {
            base.Update();
            DecideOnAttack();
            if (FightingOpponent == null)
            {
                return;
            }

            var distanceToOpponent = Vector3.Distance(characterTransform.position, FightingOpponent.Position);
            if (distanceToOpponent > SoldierRules.PawnFightDistance)
            {
                characterAnimator.SetState(CharacterAnimator.CharacterAnimState.Walking);
                characterTransform.position = Vector3.MoveTowards(characterTransform.position, FightingOpponent.Position,
                    MoveSpeed * Time.deltaTime);
                var lookAtRotation = characterLookAt.GetLookAtRotation(FightingOpponent.Position);
                characterTransform.rotation = Quaternion.RotateTowards(characterTransform.rotation,
                    lookAtRotation, CharacterGeneralConfig.CharacterRotateSpeed * Time.deltaTime);
                return;
            }
            characterAnimator.SetState(CharacterAnimator.CharacterAnimState.Attacking);
            FightingOpponent.Damage(thisSoldier.SoldierData.attack);
        }

        private void DecideOnAttack()
        {
            if (FightingOpponent != null)
            {
                if (Sight.OpponentsOnSight.Contains(FightingOpponent) == false)
                {
                    StopAttacking();
                }
                else
                {
                    return;
                }
            }
            if (Sight.OpponentsOnSight == null || Sight.OpponentsOnSight.Any() == false)
            {
                return;
            }
            StartAttacking();
        }

        private IMonoSoldier FindNearestOpponent()
        {
            return Sight.OpponentsOnSight.OrderBy(o =>
                Vector3.Distance(characterTransform.position,o.Position)).First();
        }

        protected virtual void StartAttacking()
        {
            FightingOpponent = FindNearestOpponent();
        }
        
        protected virtual void StopAttacking()
        {
            FightingOpponent = null;
        }
    }
}