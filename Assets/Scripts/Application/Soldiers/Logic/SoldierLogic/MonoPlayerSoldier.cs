﻿using TowerDefence.Enterprise.Entities;
using TowerDefence.Enterprise.Enums;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.SoldierLogic
{
    internal class MonoPlayerSoldier : MonoSoldier<PlayerSoldier>
    {
        public override SoldierRoles SoldierRole => SoldierRoles.PlayerSoldier;

        public override void Initialize(PlayerSoldier initialData)
        {
            base.Initialize(initialData);
            if (initialData.spawnPosition != default)
            {
                Data.spawnPosition = initialData.spawnPosition;
                transform.position = Data.spawnPosition;
            }
            gameObject.tag = Enterprise.Rules.TagsAndLayersRules.PlayerSoldierTag;
            gameObject.layer = LayerMask.NameToLayer(Enterprise.Rules.TagsAndLayersRules.CharacterLayer);
        }
    }
}