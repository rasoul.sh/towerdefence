﻿using TowerDefence.Enterprise.AbstractEntities;
using TowerDefence.Enterprise.Enums;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Logic.SoldierLogic
{
    public interface IMonoSoldier
    {
        Soldier SoldierData { get; }
        Vector3 Position { get; }
        Quaternion Rotation { get; }
        SoldierRoles SoldierRole { get; }
        GameObject ThisGameObject { get; }
        event SoldierDelegate OnHpChanged;
        event SoldierDelegate OnDied;
        void DieImmediate();
        public delegate void SoldierDelegate(IMonoSoldier soldier);
        TC AddComponent<TC>() where TC : Component;
        void Damage(float attack);
    }
}