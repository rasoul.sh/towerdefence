﻿using TowerDefence.Application.Soldiers.Services;

namespace TowerDefence.Application.Soldiers.Logic.SoldierLogic
{
    public interface IMonoEnemySoldier : IMonoSoldier
    {
        public event EnemySoldierDelegate OnReachedEndPoint;
        public delegate void EnemySoldierDelegate(IMonoEnemySoldier enemySoldier);
    }
}