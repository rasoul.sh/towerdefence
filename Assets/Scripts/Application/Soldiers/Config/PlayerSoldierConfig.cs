﻿using System.Collections.Generic;
using TowerDefence.Application.Soldiers.Logic.SoldierLogic;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Config
{
    [CreateAssetMenu]
    internal class PlayerSoldierConfig : ScriptableObject
    {
        [SerializeField] private MonoPlayerSoldier[] soldiers;
        public IEnumerable<MonoPlayerSoldier> Soldiers => soldiers;
    }
}