﻿using System.Collections.Generic;
using TowerDefence.Application.Soldiers.Logic.SoldierLogic;
using UnityEngine;

namespace TowerDefence.Application.Soldiers.Config
{
    [CreateAssetMenu]
    internal class EnemySoldierConfig : ScriptableObject
    {
        [SerializeField] private MonoEnemySoldier[] soldiers;
        public IEnumerable<MonoEnemySoldier> Soldiers => soldiers;
    }
}