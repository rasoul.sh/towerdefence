﻿using System;
using System.Collections;
using System.Linq;
using TowerDefence.Application.Soldiers.Services;
using UnityEngine;
using Zenject;

namespace TowerDefence.Application.Soldiers.Tests
{
    internal class TestCloneEnemies : MonoBehaviour
    {
        [Inject] private ISoldierService _solderService;
        [SerializeField] private float spawnInterval = 5f;
        [SerializeField] private int maxEnemies;
        private IEnumerator Start()
        {
            for (int i = 0; i < maxEnemies; i++)
            {
                yield return new WaitForSeconds(spawnInterval);
                _solderService.CloneEnemySoldier(_solderService.EnemyPrefabs.First().SoldierData.prefabId);
            }
        }
    }
}