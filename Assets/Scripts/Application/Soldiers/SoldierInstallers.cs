﻿using TowerDefence.Application.Soldiers.Services;
using UnityEngine;
using Zenject;

namespace TowerDefence.Application.Soldiers
{
    internal class SoldierInstallers : MonoInstaller
    {
        [SerializeField] private SoldierService soldierService;
        public override void InstallBindings()
        {
            Container.Bind<ISoldierService>().To<SoldierService>().FromInstance(soldierService).AsSingle();
        }
    }
}