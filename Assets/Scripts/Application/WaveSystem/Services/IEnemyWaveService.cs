using System;
using System.Collections;
using TowerDefence.Enterprise.Entities;

namespace TowerDefence.Application.WaveSystem.Services
{
    public interface IEnemyWaveService
    {
        IEnumerator StartWaveRoutine(EnemyWave wave);
        void StopCurrentWave();
        bool IsWaveRunning { get; }
    }
}
