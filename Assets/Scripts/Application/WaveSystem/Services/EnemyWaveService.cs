﻿using System.Collections;
using TowerDefence.Application.Soldiers.Services;
using TowerDefence.Enterprise.Entities;
using UnityEngine;
using Zenject;

namespace TowerDefence.Application.WaveSystem.Services
{
    internal class EnemyWaveService : MonoBehaviour, IEnemyWaveService
    {
        [Inject] private ISoldierService _soldierService;
        private EnemyWave _currentWave;
        public bool IsWaveRunning => _currentWave != null;

        public IEnumerator StartWaveRoutine(EnemyWave wave)
        {
            if (_currentWave != null)
            {
                Debug.LogError("You cannot start a new wave while there is a running wave already");
                yield break;
            }
            _currentWave = wave;
            var enemiesCount = _currentWave.enemies.Count;
            var interval = _currentWave.interval;
            for (int i = 0; i < enemiesCount; i++)
            {
                _soldierService.CloneEnemySoldier(_currentWave.enemies[i].PrefabId);
                yield return new WaitForSeconds(interval);
            }
            _currentWave = null;
        }

        public void StopCurrentWave()
        {
            if (_currentWave == null)
            {
                Debug.LogError("There is no wave running to stop");
                return;
            }
            StopAllCoroutines();
            _currentWave = null;
        }
    }
}