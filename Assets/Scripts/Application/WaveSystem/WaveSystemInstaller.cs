﻿using TowerDefence.Application.WaveSystem.Services;
using UnityEngine;
using Zenject;

namespace TowerDefence.Application.WaveSystem
{
    public class WaveSystemInstaller : MonoInstaller
    {
        [SerializeField] private EnemyWaveService enemyWaveService;
        
        public override void InstallBindings()
        {
            Container.Bind<IEnemyWaveService>().To<EnemyWaveService>().FromInstance(enemyWaveService).AsSingle();
        }
    }
}