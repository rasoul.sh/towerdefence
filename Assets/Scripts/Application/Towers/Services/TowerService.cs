﻿using System.Collections.Generic;
using System.Linq;
using TowerDefence.Application.Towers.Config;
using TowerDefence.Application.Towers.Logic.TowerLogic;
using TowerDefence.Core.GameAssetBase;
using TowerDefence.Enterprise.Entities;
using UnityEngine;

namespace TowerDefence.Application.Towers.Services
{
    internal class TowerService : MonoBehaviour, ITowerService
    {
        [SerializeField] private TowerConfig config;

        public IEnumerable<Tower> TowerPrefabs => config.TowerPrefabs.Select(tp => tp.Data);
        public event ITowerService.TowerDelegate OnTowerChanged;
        private List<MonoTower> _currentTowers;

        private void Start()
        {
            _currentTowers = new List<MonoTower>();
        }

        public IGameAssetBehaviour<Tower> ConstructTower(string prefabId, Vector3 position, Quaternion rotation)
        {
            var prefab = config.TowerPrefabs.First(tp => tp.PrefabId == prefabId);
            var newTower = prefab.Instantiate() as MonoTower;
            var newTowerTransform = newTower.transform;
            newTowerTransform.position = position;
            newTowerTransform.rotation = rotation;
            newTower.Initialize(newTower.Data);
            _currentTowers.Add(newTower);
            return newTower;
        }

        public void UpgradeTower(string towerId)
        {
            var tower = GetTower(towerId);
            (tower as MonoTower).SetLevel(tower.Data.currentLevelIndex + 1);
            OnTowerChanged?.Invoke(tower.Data);
        }

        public Tower GetTowerData(string towerId)
        {
            var tower = GetTower(towerId);
            return tower.Data;
        }

        public void DestructAllTowers()
        {
            for (int i = 0; i < _currentTowers.Count; i++)
            {
                _currentTowers[i].Destroy();
                i--;
            }
        }

        public IGameAssetBehaviour<Tower> GetTower(string towerId)
        {
            return _currentTowers.First(t => t.Data.id == towerId);
        }
    }
}