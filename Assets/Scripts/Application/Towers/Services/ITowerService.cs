﻿using System.Collections.Generic;
using TowerDefence.Core.GameAssetBase;
using TowerDefence.Enterprise.Entities;
using UnityEngine;

namespace TowerDefence.Application.Towers.Services
{
    public interface ITowerService
    {
        IEnumerable<Tower> TowerPrefabs { get; }
        event TowerDelegate OnTowerChanged;
        IGameAssetBehaviour<Tower> ConstructTower(string prefabId, Vector3 position, Quaternion rotation);
        void UpgradeTower(string towerId);
        IGameAssetBehaviour<Tower> GetTower(string towerId);
        Tower GetTowerData(string towerId);
        void DestructAllTowers();
        
        public delegate void TowerDelegate(Tower tower);
    }
}