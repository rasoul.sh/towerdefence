﻿using TowerDefence.Application.Towers.Services;
using UnityEngine;
using Zenject;

namespace TowerDefence.Application.Towers
{
    internal class TowerInstaller : MonoInstaller
    {
        [SerializeField] private TowerService towerService;
        
        public override void InstallBindings()
        {
            Container.Bind<ITowerService>().To<TowerService>().FromInstance(towerService).AsSingle();
        }
    }
}