using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TowerDefence.Application.Soldiers.Logic.SoldierLogic;
using TowerDefence.Application.Soldiers.Services;
using TowerDefence.Core.GameAssetBase;
using TowerDefence.Enterprise.Entities;
using UnityEngine;
using Zenject;

namespace TowerDefence.Application.Towers.Logic.TowerLogic
{
    internal class MonoTower : GameAssetBehaviour<Tower>
    {
        [Inject] private ISoldierService _soldierService;
        [SerializeField] private GameAssetBehaviour soldierPrefab;
        [SerializeField] private TowerSpawnPoint[] spawnPoints;
        private List<IMonoSoldier> _currentSoldiers;

        public override void Initialize(Tower initialData)
        {
            base.Initialize(initialData);
            _currentSoldiers = new List<IMonoSoldier>();
            for (int i = 0; i < initialData.capacity; i++)
            {
                SpawnSoldier();
            }
            SetLevel(0);
        }

        private void SpawnSoldier()
        {
            var spawnPoint = spawnPoints.FirstOrDefault(sp => string.IsNullOrEmpty(sp.SoldierId));
            if (spawnPoint == null)
            {
                Debug.LogError("Cannot respawn the solider while all the spawn points are occupied");
                return;
            }

            var playerPrefab = soldierPrefab as GameAssetBehaviour<PlayerSoldier>;
            var newSoldier = _soldierService.ClonePlayerSoldier(playerPrefab.PrefabId);
            var playerSoldier = newSoldier as GameAssetBehaviour<PlayerSoldier>;
            playerSoldier.Data.attackRange = Data.CurrentLevel.maxRange;
            playerSoldier.Data.spawnPosition = spawnPoint.transform.position;
            playerSoldier.transform.position = spawnPoint.transform.position;
            playerSoldier.transform.rotation = spawnPoint.transform.rotation;
            spawnPoint.SoldierId = newSoldier.SoldierData.id;
            _currentSoldiers.Add(newSoldier);
            newSoldier.OnDied += OnSoldierDied;
        }

        private void OnSoldierDied(IMonoSoldier soldier)
        {
            _currentSoldiers.Remove(soldier);
            spawnPoints.First(sp => sp.SoldierId == soldier.SoldierData.id).SoldierId = null;
            StartCoroutine(SoldierRespawnRoutine());
        }

        private IEnumerator SoldierRespawnRoutine()
        {
            yield return new WaitForSeconds(Data.respawnDelay);
            SpawnSoldier();
        }

        public void SetLevel(int level)
        {
            Data.currentLevelIndex = level;
            var thisTransform = transform;
            var childCount = thisTransform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                thisTransform.GetChild(i).gameObject.SetActive(i == level);
            }

            foreach (var soldier in _currentSoldiers)
            {
                soldier.SoldierData.attackRange = Data.CurrentLevel.maxRange;
            }
        }
    }
}
