﻿using System.Collections.Generic;
using TowerDefence.Application.Towers.Logic.TowerLogic;
using UnityEngine;

namespace TowerDefence.Application.Towers.Config
{
    [CreateAssetMenu]
    internal class TowerConfig : ScriptableObject
    {
        [SerializeField] private MonoTower[] towerPrefabs;
        public IEnumerable<MonoTower> TowerPrefabs => towerPrefabs;
    }
}