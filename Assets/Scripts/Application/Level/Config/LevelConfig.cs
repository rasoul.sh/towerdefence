using TowerDefence.Enterprise.Entities;
using UnityEngine;

namespace TowerDefence.Application.Level.Config
{
    [CreateAssetMenu]
    internal class LevelConfig : ScriptableObject
    {
        [SerializeField] private LevelData[] levels;
        public LevelData[] Levels => levels;
    }
}
