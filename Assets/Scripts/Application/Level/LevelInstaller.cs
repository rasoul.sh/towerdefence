﻿using TowerDefence.Application.Level.Services;
using UnityEngine;
using Zenject;

namespace TowerDefence.Application.Level
{
    internal class LevelInstaller : MonoInstaller
    {
        [SerializeField] private LevelService levelService;

        public override void InstallBindings()
        {
            Container.Bind<ILevelService>().To<LevelService>().FromInstance(levelService).AsSingle();
        }
    }
}