﻿using System.Collections.Generic;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Enterprise.Enums;

namespace TowerDefence.Application.Level.Services
{
    public interface ILevelService
    {
        IEnumerable<LevelData> Levels { get; }
        LevelData CurrentLevel { get; }
        int CurrentLevelIndex { get; }
        bool IsOnLastLevel { get; }
        event LivesDelegate OnLivesChanged;
        event LevelFinishDelegate OnLevelFinished;
        event LevelQuitDelegate OnLevelQuited;
        event GameSpeedDelegate OnGameSpeedChanged;
        event WaveDelegate OnWaveComing;
        event WaveIndexDelegate OnWaveStarted;
        void StartLevel(int levelIndex);
        void RestartLevel();
        void QuitLevel(QuitLevelReason quitReason);
        void NextLevel();
        void PauseGame();
        void ResumeGame();
        void QuitGame();
        void DecreaseLives();
        void SetGameSpeed(GameSpeeds gameSpeed);

        public delegate void GameSpeedDelegate(GameSpeeds gameSpeed);
        public delegate void LivesDelegate(int lives);

        public delegate void LevelFinishDelegate(LevelData levelData);

        public delegate void LevelQuitDelegate(QuitLevelReason quitReason);
        public delegate void WaveDelegate(int waveIndex);
        public delegate void WaveIndexDelegate(int waveIndex, bool isFinalWave);
    }
}