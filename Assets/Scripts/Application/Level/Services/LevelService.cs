﻿using System.Collections;
using System.Collections.Generic;
using TowerDefence.Application.GameResources.Services;
using TowerDefence.Application.Level.Config;
using TowerDefence.Application.Soldiers.Logic.SoldierLogic;
using TowerDefence.Application.Soldiers.Services;
using TowerDefence.Application.WaveSystem.Services;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Enterprise.Enums;
using UnityEngine;
using Zenject;

namespace TowerDefence.Application.Level.Services
{
    internal class LevelService : MonoBehaviour, ILevelService
    {
        [Inject] private IEnemyWaveService _enemyWaveService;
        [Inject] private ISoldierService _soldierService;
        [Inject] private IGameResourceService _gameResourceService;
        [SerializeField] private LevelConfig levelConfig;
        public IEnumerable<LevelData> Levels => levelConfig.Levels;
        public LevelData CurrentLevel { get; private set; }
        public event ILevelService.LivesDelegate OnLivesChanged;
        public event ILevelService.LevelFinishDelegate OnLevelFinished;
        public event ILevelService.LevelQuitDelegate OnLevelQuited;
        public event ILevelService.GameSpeedDelegate OnGameSpeedChanged;
        public event ILevelService.WaveDelegate OnWaveComing;
        public event ILevelService.WaveIndexDelegate OnWaveStarted;
        public int CurrentLevelIndex { get; private set; } = -1;
        public bool IsOnLastLevel => CurrentLevelIndex >= levelConfig.Levels.Length - 1;
        private List<Coroutine> _playingRoutines;

        private void Start()
        {
            _playingRoutines = new();
            _soldierService.OnSoldierDied += OnSoldierDied;
            _soldierService.OnEnemyReachedEndPoint += OnEnemyReachedEndPoint;
        }

        private void OnEnemyReachedEndPoint(IMonoEnemySoldier enemySoldier)
        {
            DecreaseLives();
        }

        private void OnSoldierDied(IMonoSoldier soldier)
        {
            if (CurrentLevelIndex < 0)
            {
                return;
            }
            if (soldier.SoldierRole == SoldierRoles.EnemySoldier)
            {
                _gameResourceService.IncreaseGold(CurrentLevel.CurrentWave.goldPrize);
                _gameResourceService.IncreaseScore(CurrentLevel.CurrentWave.scorePrize);
            }
        }

        public void StartLevel(int levelIndex)
        {
            if (levelIndex < 0)
            {
                Debug.LogError("Level index couldn't be less than 0");
                return;
            }
            if (levelIndex > levelConfig.Levels.Length - 1)
            {
                Debug.LogError("Level index is out of range");
                return;
            }
            if (CurrentLevelIndex > -1)
            {
                QuitLevel(QuitLevelReason.LevelChanged);
            }
            _gameResourceService.CurrentCatalog = new ResourceCatalog();
            _gameResourceService.ResetResources();
            CurrentLevelIndex = levelIndex;
            CurrentLevel = levelConfig.Levels[levelIndex].Instantiate();
            _gameResourceService.IncreaseGold(CurrentLevel.startingGold);
            _playingRoutines.Add(StartCoroutine(PlayLevelRoutine()));
            SetGameSpeed(GameSpeeds.NormalSpeed);
        }

        public void RestartLevel()
        {
            if (CurrentLevelIndex < 0)
            {
                Debug.LogError("There is not any level to restart");
                return;
            }

            var levelIndex = CurrentLevelIndex;
            QuitLevel(QuitLevelReason.Restarted);
            StartLevel(levelIndex);
        }

        public void QuitLevel(QuitLevelReason quitReason)
        {
            if (CurrentLevelIndex < 0)
            {
                Debug.LogError("There is not any playing level to quit");
                return;
            }

            foreach (var routine in _playingRoutines)
            {
                StopCoroutine(routine);
            }
            _playingRoutines.Clear();
            if (_enemyWaveService.IsWaveRunning)
            {
                _enemyWaveService.StopCurrentWave();
            }
            _soldierService.DestroyAllSoldiers();
            CurrentLevelIndex = -1;
            CurrentLevel = null;
            OnLevelQuited?.Invoke(quitReason);
        }

        public void NextLevel()
        {
            if (CurrentLevelIndex < 0)
            {
                Debug.LogError("Cannot play next level while no level is playing yet");
                return;
            }
            if (IsOnLastLevel)
            {
                Debug.LogError("Cannot play next level while the current level is the last level");
                return;
            }
            QuitLevel(QuitLevelReason.LevelChanged);
            StartLevel(CurrentLevelIndex + 1);
        }

        public void PauseGame()
        {
            Time.timeScale = 0;
        }

        public void ResumeGame()
        {
            Time.timeScale = (int)CurrentLevel.currentGameSpeed;
        }

        public void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            return;
#endif
            UnityEngine.Application.Quit();
        }

        public void DecreaseLives()
        {
            if (CurrentLevelIndex < 0)
            {
                Debug.LogError("Cannot decrease lives while there is not any level playing");
                return;
            }
            if (CurrentLevel.currentLives < 1)
            {
                Debug.LogError("Lives amount is zero already, cannot decrease more lives");
                return;
            }

            if (--CurrentLevel.currentLives < 1)
            {
                Defeat();
                QuitGame();
            }
            OnLivesChanged?.Invoke(CurrentLevel.currentLives);
        }

        public void SetGameSpeed(GameSpeeds gameSpeed)
        {
            if (Time.timeScale == 0)
            {
                Debug.LogError("Cannot set game speed while the game is paused");
                return;
            }

            Time.timeScale = (int)gameSpeed;
            CurrentLevel.currentGameSpeed = gameSpeed;
            OnGameSpeedChanged?.Invoke(gameSpeed);
        }

        private void Defeat()
        {
            if (CurrentLevelIndex < 0)
            {
                Debug.LogError("There is not playing level to set it as defeated");
                return;
            }

            if (CurrentLevel.currentLevelState != LevelState.Playing)
            {
                Debug.LogError("The level has been finished already, you cannot set it as defeated");
                return;
            }

            CurrentLevel.currentLevelState = LevelState.Defeated;
            OnLevelFinished?.Invoke(CurrentLevel);
            
        }

        private IEnumerator PlayLevelRoutine()
        {
            var levelPrefab = levelConfig.Levels[CurrentLevelIndex];
            var wavesCount = levelPrefab.enemyWaves.Length;
            for (int i = 0; i < wavesCount; i++)
            {
                OnWaveComing?.Invoke(i);
                yield return new WaitForSeconds(levelPrefab.wavesInterval);
                OnWaveStarted?.Invoke(i, i > wavesCount - 2);
                CurrentLevel.currentWaveIndex = i;
                var wave = levelPrefab.enemyWaves[i];
                var routine = StartCoroutine(_enemyWaveService.StartWaveRoutine(wave));
                _playingRoutines.Add(routine);
                yield return routine;
                _playingRoutines.Remove(routine);
            }
        }
    }
}