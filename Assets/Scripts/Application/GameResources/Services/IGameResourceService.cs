﻿using TowerDefence.Enterprise.Entities;

namespace TowerDefence.Application.GameResources.Services
{
    public interface IGameResourceService
    {
        ResourceCatalog CurrentCatalog { set; }
        int CurrentGolds { get; }
        int CurrentScore { get; }
        event ResourceDelegate OnGoldAmountChanged;
        event ResourceDelegate OnScoreAmountChanged;
        void IncreaseGold(int amount);
        bool DecreaseGold(int amount);
        void IncreaseScore(int amount);
        void ResetResources();
        
        delegate void ResourceDelegate(int amount);
    }
}