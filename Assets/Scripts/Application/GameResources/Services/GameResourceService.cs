using JetBrains.Annotations;
using TowerDefence.Enterprise.Entities;
using UnityEngine;

namespace TowerDefence.Application.GameResources.Services
{
    [UsedImplicitly]
    internal class GameResourceService : IGameResourceService
    {
        public ResourceCatalog CurrentCatalog { private get; set; }
        public int CurrentGolds => CurrentCatalog?.golds ?? 0;
        public int CurrentScore => CurrentCatalog?.scores ?? 0;
        public event IGameResourceService.ResourceDelegate OnGoldAmountChanged;
        public event IGameResourceService.ResourceDelegate OnScoreAmountChanged;

        public void IncreaseGold(int amount)
        {
            SetGolds(CurrentCatalog.golds + amount);

        }

        public bool DecreaseGold(int amount)
        {
            return SetGolds(CurrentCatalog.golds - amount);
        }

        public void IncreaseScore(int amount)
        {
            SetScore(CurrentCatalog.scores + amount);
        }

        private bool SetGolds(int golds)
        {
            if (golds < 0)
            {
                Debug.LogError("Cannot set golds to a negative value");
                return false;
            }
            CurrentCatalog.golds = golds;
            OnGoldAmountChanged?.Invoke(golds);
            return true;
        }

        private void SetScore(int score)
        {
            if (score < 0)
            {
                Debug.LogError("Cannot set score to a negative value");
                return;
            }
            CurrentCatalog.scores = score;
            OnScoreAmountChanged?.Invoke(score);
        }

        public void ResetResources()
        {
            SetGolds(0);
            SetScore(0);
        }
    }
}
