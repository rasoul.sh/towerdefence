﻿using TowerDefence.Application.GameResources.Services;
using Zenject;

namespace TowerDefence.Application.GameResources
{
    internal class GameResourceInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IGameResourceService>().To<GameResourceService>().FromNew().AsSingle();
        }
    }
}