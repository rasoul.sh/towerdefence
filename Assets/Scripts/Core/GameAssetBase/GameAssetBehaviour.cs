﻿using TowerDefence.Core.EntityBase;
using UnityEngine;
using Zenject;

namespace TowerDefence.Core.GameAssetBase
{
    [RequireComponent(typeof(Collider))]
    public abstract class GameAssetBehaviour<T> : GameAssetBehaviour, IGameAssetBehaviour<T> where T : MonoEntity
    {
        [Inject] private DiContainer _container;
        [SerializeField] private T data;
        [SerializeField] private bool initializeOnAwake;
        private BoxCollider _boundaryCollider;
        private Transform _transform;
        public virtual T Data => data;
        public override string PrefabId => Data.prefabId;

        public bool IsInstance { get; private set; }
        public event IGameAssetBehaviour<T>.GameObjectBehaviourDelegate OnDestroying;

        protected virtual void Awake()
        {
            if (initializeOnAwake == false)
            {
                return;
            }
            Data.ConvertToInstance();
            IsInstance = true;
            Initialize(data);
        }

        public TC AddComponent<TC>() where TC : Component
        {
            return _container.InstantiateComponent<TC>(gameObject);
        }

        public IGameAssetBehaviour<T> Instantiate()
        {
            var instance = GameAssetCreator.Instance.Instantiate(this);
            instance.Data.ConvertToInstance();
            instance.IsInstance = true;
            return instance;
        }

        public virtual void Initialize(T initialData)
        {
            if (initialData == null)
            {
                Debug.LogError("You are trying to initialize a game object using null data", this);
                return;
            }
            if (initialData.isInstance == false)
            {
                Debug.LogError("You are trying to initialize a game object that is not an instance", this);
                return;
            }
            data = initialData;
        }

        public void Destroy()
        {
            if (IsInstance == false)
            {
                return;
            }
            OnDestroying?.Invoke(this);
            Destroy(gameObject);
        }

        public GameObject CloneFakeInstance()
        {
            var fakeInstance = Instantiate(this).gameObject;
            Destroy(fakeInstance.GetComponent(GetType()));
            return fakeInstance;
        }
    }
    
    public abstract class GameAssetBehaviour : MonoBehaviour
    {
        public abstract string PrefabId { get; }
    }
}