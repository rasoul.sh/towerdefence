﻿using JetBrains.Annotations;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace TowerDefence.Core.GameAssetBase
{
    internal class GameAssetCreator : MonoBehaviour
    {
        [Inject] private DiContainer _container;
        internal static GameAssetCreator Instance { get; private set; }

        private void Start()
        {
            Instance = this;
        }

        public new T Instantiate<T>(T original) where T : Object
        {
            return _container.InstantiatePrefabForComponent<T>(original);
        }
    }
}