﻿using TowerDefence.Core.EntityBase;
using UnityEngine;

namespace TowerDefence.Core.GameAssetBase
{
    public interface IGameAssetBehaviour<T> where T : MonoEntity
    {
        T Data { get; }
        bool IsInstance { get; }
        IGameAssetBehaviour<T> Instantiate();
        void Destroy();
        void Initialize(T initialData);
        event GameObjectBehaviourDelegate OnDestroying;
        public delegate void GameObjectBehaviourDelegate(IGameAssetBehaviour<T> assetBehaviour);

        TC AddComponent<TC>() where TC : Component;
        GameObject CloneFakeInstance();
    }
}