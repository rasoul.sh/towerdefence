﻿using System;
using TowerDefence.Core.UnityExtensions.Attributes.UnityHideIfEmpty;
using TowerDefence.Core.UnityExtensions.Attributes.UnityReadOnlyField;

namespace TowerDefence.Core.EntityBase
{
    public class GameEntity
    {
        [ReadOnlyField] [HideIfEmpty] public string id;
        
        protected static string GenerateGuid()
        {
            return Guid.NewGuid().ToString();
        }
    }
}