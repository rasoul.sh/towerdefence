﻿using System;
using System.Xml.Serialization;
using Newtonsoft.Json;
using TowerDefence.Core.UnityExtensions.Attributes.UnityReadOnlyField;
using UnityEngine;

namespace TowerDefence.Core.EntityBase
{
    [Serializable]
    public abstract class MonoEntity : GameEntity
    {
        [HideInInspector] public bool isInstance;
        [ReadOnlyField] public string prefabId = GenerateGuid();
        public string title;

        public void ConvertToInstance()
        {
            id = GenerateGuid();
            isInstance = true;
        }
    }
}