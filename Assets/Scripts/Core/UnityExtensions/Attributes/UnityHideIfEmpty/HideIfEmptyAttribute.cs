﻿using System;
using UnityEngine;

namespace TowerDefence.Core.UnityExtensions.Attributes.UnityHideIfEmpty
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class HideIfEmptyAttribute : PropertyAttribute { }
}