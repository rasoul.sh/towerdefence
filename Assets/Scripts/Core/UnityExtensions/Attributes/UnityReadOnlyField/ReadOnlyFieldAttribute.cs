﻿using System;
using UnityEngine;

namespace TowerDefence.Core.UnityExtensions.Attributes.UnityReadOnlyField
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ReadOnlyFieldAttribute : PropertyAttribute { }
}