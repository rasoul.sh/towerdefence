﻿namespace TowerDefence.Enterprise.Enums
{
    public enum QuitLevelReason
    {
        Finished = 0,
        Aborted = 1,
        Restarted = 2,
        LevelChanged = 3
    }
}