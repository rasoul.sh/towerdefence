﻿namespace TowerDefence.Enterprise.Enums
{
    public enum GameSpeeds
    {
        NormalSpeed = 1,
        DoubleSpeed = 2
    }
}