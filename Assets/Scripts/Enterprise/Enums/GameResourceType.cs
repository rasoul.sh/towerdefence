﻿namespace TowerDefence.Enterprise.Enums
{
    public enum GameResourceType
    {
        Gold = 0,
        Score = 1
    }
}