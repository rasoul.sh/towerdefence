﻿namespace TowerDefence.Enterprise.Enums
{
    public enum SoldierRoles
    {
        PlayerSoldier = 0,
        EnemySoldier = 1
    }
}