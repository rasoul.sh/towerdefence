﻿namespace TowerDefence.Enterprise.Enums
{
    public enum LevelState
    {
        Playing = 0,
        Defeated = 1,
        Victory = 2,
        Aborted = 3
    }
}