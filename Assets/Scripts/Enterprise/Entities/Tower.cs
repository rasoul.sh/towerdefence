﻿using System;
using System.Xml.Serialization;
using TowerDefence.Core.EntityBase;
using TowerDefence.Core.GameAssetBase;
using TowerDefence.Enterprise.Enums;
using TowerDefence.Enterprise.ValueObjects;
using Unity.Plastic.Newtonsoft.Json;
using UnityEngine;

namespace TowerDefence.Enterprise.Entities
{
    [Serializable]
    public class Tower : MonoEntity
    {
        public TowerTypes towerType;
        public TowerLevel[] levels;
        public int capacity;
        public int respawnDelay;
        public int goldCost;

        [HideInInspector] public int currentLevelIndex;
        [JsonIgnore] [XmlIgnore] public TowerLevel CurrentLevel => levels[currentLevelIndex];

        [JsonIgnore] [XmlIgnore] public bool IsLastUpgrade => currentLevelIndex >= levels.Length - 1;
    }
}