﻿using System;
using System.Xml.Serialization;
using Newtonsoft.Json;
using TowerDefence.Core.EntityBase;
using TowerDefence.Enterprise.Enums;
using UnityEngine;

namespace TowerDefence.Enterprise.Entities
{
    [Serializable]
    public class LevelData : GameEntity
    {
        public int maxLives;
        public int startingGold;
        public int wavesInterval;
        [JsonIgnore] [XmlIgnore] public EnemyWave[] enemyWaves;
        [HideInInspector] public int currentLives;
        [HideInInspector] public int currentWaveIndex = -1;
        [HideInInspector] public GameSpeeds currentGameSpeed;
        [HideInInspector] public LevelState currentLevelState;
        [JsonIgnore][XmlIgnore] public EnemyWave CurrentWave => enemyWaves[currentWaveIndex];

        public LevelData Instantiate()
        {
            var newLevel = MemberwiseClone() as LevelData;
            newLevel.id = GenerateGuid();
            newLevel.currentLives = maxLives;
            newLevel.currentGameSpeed = GameSpeeds.NormalSpeed;
            newLevel.currentLevelState = LevelState.Playing;
            return newLevel;
        }
    }
}