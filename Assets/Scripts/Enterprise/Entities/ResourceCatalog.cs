﻿using System;
using TowerDefence.Core.EntityBase;
using TowerDefence.Enterprise.Enums;
using UnityEngine;

namespace TowerDefence.Enterprise.Entities
{
    [Serializable]
    public class ResourceCatalog : GameEntity
    {
        public int golds;
        public int scores;
        
        public int GetResourceCount(GameResourceType resourceType)
        {
            switch (resourceType)
            {
                case GameResourceType.Gold:
                    return golds;
                case GameResourceType.Score:
                    return scores;
                default:
                    Debug.LogError("The resource type doesn't exist");
                    return 0;
            }
        }
        
        public void SetResourceCount(GameResourceType resourceType, int value)
        {
            switch (resourceType)
            {
                case GameResourceType.Gold:
                    golds = value;
                    break;
                case GameResourceType.Score:
                    scores = value;
                    break;
                default:
                    Debug.LogError("The resource type doesn't exist");
                    break;
            }
        }

        public void Increase(GameResourceType resourceType, int amount) =>
            SetResourceCount(resourceType, GetResourceCount(resourceType) + amount);

        public void Decrease(GameResourceType resourceType, int amount) =>
            Increase(resourceType, -amount);
    }
}