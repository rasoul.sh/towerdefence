﻿using System;
using System.Collections.Generic;
using TowerDefence.Core.EntityBase;
using TowerDefence.Core.GameAssetBase;

namespace TowerDefence.Enterprise.Entities
{
    [Serializable]
    public class EnemyWave : GameEntity
    {
        public List<GameAssetBehaviour> enemies;
        public float interval;
        public int goldPrize;
        public int scorePrize;
    }
}