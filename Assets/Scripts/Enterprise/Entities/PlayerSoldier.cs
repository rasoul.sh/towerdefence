﻿using System;
using TowerDefence.Enterprise.AbstractEntities;
using UnityEngine;

namespace TowerDefence.Enterprise.Entities
{
    [Serializable]
    public class PlayerSoldier : Soldier
    {
        [HideInInspector] public Vector3 spawnPosition;
    }
}