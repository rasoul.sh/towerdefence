﻿using System;

namespace TowerDefence.Enterprise.ValueObjects
{
    [Serializable]
    public class TowerLevel
    {
        public int maxRange;
        public int scoreNeeded;
        public int upgradeCost;
    }
}