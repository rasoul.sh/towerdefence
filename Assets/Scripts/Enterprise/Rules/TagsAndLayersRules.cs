﻿namespace TowerDefence.Enterprise.Rules
{
    public static class TagsAndLayersRules
    {
        public const string CharacterLayer = "Character";
        public const string PlayerSoldierTag = "PlayerSoldier";
        public const string EnemySoldierTag = "EnemySoldier";
    }
}