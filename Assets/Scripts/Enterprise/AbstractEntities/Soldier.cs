﻿using System;
using System.Collections.Generic;
using TowerDefence.Core.EntityBase;
using TowerDefence.Enterprise.Enums;
using UnityEngine;

namespace TowerDefence.Enterprise.AbstractEntities
{
    [Serializable]
    public abstract class Soldier : MonoEntity
    {
        public SoldierTypes soldierType;
        public int maxHp;
        public int moveSpeed;
        public int attack;
        public bool isOnTower;
        public float attackRange;
        [HideInInspector] public float currentHp;

        public void Revive()
        {
            currentHp = maxHp;
        }
    }
}