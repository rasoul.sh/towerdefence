﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using TowerDefence.Application.GameResources.Services;
using TowerDefence.Application.Towers.Services;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Interactor.Common.Mapper;
using TowerDefence.Interactor.ViewModels;
using UnityEngine;
using Zenject;

namespace TowerDefence.Interactor.Controllers
{
    [UsedImplicitly]
    public class TowerController : ITowerController
    {
        [Inject] private ITowerService _towerService;
        [Inject] private IGameResourceService _gameResourceService;
        [Inject] private IReadOnlyMapper<Tower, TowerPrefab> _towerPrefabMapper;
        [Inject] private IReadOnlyMapper<Tower, TowerInfo> _towerInfoMapper;

        public IEnumerable<TowerPrefab> AllTowerPrefabs => _towerService.TowerPrefabs.Select(_towerPrefabMapper.MapToTarget);
            
        public string BuyTower(string prefabId, Vector3 position, Quaternion rotation)
        {
            var towerPrefab = _towerService.TowerPrefabs.First(tp => tp.prefabId == prefabId);
            if (_gameResourceService.DecreaseGold(towerPrefab.goldCost) == false)
            {
                Debug.LogError("InsufficientGold");
                return null;
            }
            var newTower = _towerService.ConstructTower(prefabId, position, rotation);
            return newTower.Data.id;
        }

        public void UpgradeTower(string towerId)
        {
            var towerData = _towerService.GetTowerData(towerId);
            var nextLevel = towerData.levels[towerData.currentLevelIndex + 1];
            if (_gameResourceService.DecreaseGold(nextLevel.upgradeCost) == false)
            {
                Debug.LogError("InsufficientGold");
                return;
            }
            if (_gameResourceService.CurrentScore < nextLevel.scoreNeeded)
            {
                Debug.LogError("InsufficientScore");
                return;
            }
            _towerService.UpgradeTower(towerId);
        }

        public TowerInfo GetTowerInfo(string towerId)
        {
            var towerData = _towerService.GetTowerData(towerId);
            return _towerInfoMapper.MapToTarget(towerData);
        }

        public TC AddComponentToTower<TC>(string towerId) where TC : Component
        {
            var tower = _towerService.GetTower(towerId);
            return tower.AddComponent<TC>();
        }
    }
}