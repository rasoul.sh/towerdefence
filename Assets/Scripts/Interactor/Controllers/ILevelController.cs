using TowerDefence.Enterprise.Enums;
using TowerDefence.Interactor.ViewModels;
using UnityEngine;

namespace TowerDefence.Interactor.Controllers
{
    public interface ILevelController
    {
        LevelList AllLevels { get; }
        GameSpeeds CurrentGameSpeed { get; }
        int CurrentLives { get; }
        void SelectLevel(int levelNumber);
        void SetGameSpeed(GameSpeeds gameSpeed);
        void QuitGame();
        void PauseGame();
        void ResumeGame();
        void RestartLevel();
        void QuitLevel();
        event QuitDelegate OnQuit;
        event LevelFinishDelegate OnFinish;
        event LivesDelegate OnLivesChanged;
        event GameSpeedDelegate OnGameSpeedChanged;
        event WaveDelegate OnWaveComing;
        event WaveNumberDelegate OnWaveStarted;
        public delegate void QuitDelegate(QuitLevelReason quitLevelReason);
        public delegate void LevelFinishDelegate(LevelResult levelResult);
        public delegate void LivesDelegate(int lives);
        public delegate void GameSpeedDelegate(GameSpeeds gameSpeed);
        public delegate void WaveDelegate(int waveNumber);
        public delegate void WaveNumberDelegate(int waveNumber, bool isFinalWave);

    }
   
}