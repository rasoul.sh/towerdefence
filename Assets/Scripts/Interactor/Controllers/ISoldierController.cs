﻿using UnityEngine;

namespace TowerDefence.Interactor.Controllers
{
    public interface ISoldierController
    {
        event SoldierDelegate OnSoldierSpawned;
        event SoldierIdDelegate OnSoldierDestroyed;
        event HpValueDelegate OnHpValueChanged;

        public delegate void SoldierDelegate(Transform soldier, string soldierId);
        public delegate void SoldierIdDelegate(string soldierId);
        public delegate void HpValueDelegate(string soldierId, float value);
    }
}