﻿namespace TowerDefence.Interactor.Controllers
{
    public interface IGameResourceController
    {
        int CurrentGolds { get; }
        int CurrentScore { get; }
        event GoldDelegate OnGoldsAmountChanged;
        event ScoreDelegate OnScoreAmountChanged;

        public delegate void GoldDelegate(int golds);

        public delegate void ScoreDelegate(int score);
    }
}