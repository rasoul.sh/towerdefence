﻿using System.Collections.Generic;
using JetBrains.Annotations;
using TowerDefence.Application.Level.Services;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Enterprise.Enums;
using TowerDefence.Interactor.Common.Mapper;
using TowerDefence.Interactor.ViewModels;
using Zenject;

namespace TowerDefence.Interactor.Controllers
{
    [UsedImplicitly]
    internal class LevelController : ILevelController
    {
        private ILevelService _levelService;
        
        //mappers injection
        [Inject] private IReadOnlyMapper<IEnumerable<LevelData>, LevelList> _levelListMapper;
        [Inject] private IReadOnlyMapper<LevelData, LevelResult> _levelResultMapper;

        public LevelList AllLevels => _levelListMapper.MapToTarget(_levelService.Levels);
        public GameSpeeds CurrentGameSpeed => _levelService.CurrentLevel?.currentGameSpeed ?? GameSpeeds.NormalSpeed;
        public int CurrentLives => _levelService.CurrentLevel?.currentLives ?? 0;

        public event ILevelController.QuitDelegate OnQuit;
        public event ILevelController.LevelFinishDelegate OnFinish;
        public event ILevelController.LivesDelegate OnLivesChanged;
        public event ILevelController.GameSpeedDelegate OnGameSpeedChanged;
        public event ILevelController.WaveDelegate OnWaveComing;
        public event ILevelController.WaveNumberDelegate OnWaveStarted;

        [Inject]
        private void Init(ILevelService levelService)
        {
            _levelService = levelService;
            _levelService.OnLevelQuited += delegate(QuitLevelReason reason) { OnQuit?.Invoke(reason); };
            _levelService.OnLevelFinished += delegate(LevelData data) { OnFinish?.Invoke(_levelResultMapper.MapToTarget(data)); };
            _levelService.OnLivesChanged += delegate(int lives) { OnLivesChanged?.Invoke(lives); };
            _levelService.OnGameSpeedChanged += delegate(GameSpeeds speed) { OnGameSpeedChanged?.Invoke(speed); };
            _levelService.OnWaveComing += delegate(int waveIndex) { OnWaveComing?.Invoke(waveIndex + 1); };
            _levelService.OnWaveStarted += delegate(int waveIndex, bool isFinalWave) { OnWaveStarted?.Invoke(waveIndex + 1, isFinalWave); };
        }
        
        public void SelectLevel(int levelNumber)
        {
            _levelService.StartLevel(levelNumber - 1);
        }

        public void SetGameSpeed(GameSpeeds gameSpeed)
        {
            _levelService.SetGameSpeed(gameSpeed);
        }

        public void QuitGame()
        {
            _levelService.QuitGame();
        }
        
        public void QuitLevel()
        {
            _levelService.QuitLevel(QuitLevelReason.Aborted);
        }

        public void PauseGame()
        {
            _levelService.PauseGame();
        }

        public void ResumeGame()
        {
            _levelService.ResumeGame();
        }
        
        public void RestartLevel()
        {
            _levelService.RestartLevel();
        }
    }
}