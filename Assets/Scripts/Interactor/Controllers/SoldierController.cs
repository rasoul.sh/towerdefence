﻿using JetBrains.Annotations;
using TowerDefence.Application.Soldiers.Logic.SoldierLogic;
using TowerDefence.Application.Soldiers.Services;
using TowerDefence.Core.GameAssetBase;
using TowerDefence.Enterprise.AbstractEntities;
using Zenject;

namespace TowerDefence.Interactor.Controllers
{
    [UsedImplicitly]
    public class SoldierController : ISoldierController
    {
        public event ISoldierController.SoldierDelegate OnSoldierSpawned;
        public event ISoldierController.SoldierIdDelegate OnSoldierDestroyed;
        public event ISoldierController.HpValueDelegate OnHpValueChanged;

        [Inject]
        private void Init(ISoldierService soldierService)
        {
            soldierService.OnSoldierSpawned += delegate(IMonoSoldier soldier)
            {
                OnSoldierSpawned?.Invoke(soldier.ThisGameObject.transform, soldier.SoldierData.id);
            };
            soldierService.OnSoldierDestroyed += delegate(IMonoSoldier soldier)
            {
                OnSoldierDestroyed?.Invoke(soldier.SoldierData.id);
            };
            soldierService.OnSoldierHpChanged += delegate(IMonoSoldier soldier)
            {
                OnHpValueChanged?.Invoke(soldier.SoldierData.id, soldier.SoldierData.currentHp / soldier.SoldierData.maxHp);
            };
        }
    }
}