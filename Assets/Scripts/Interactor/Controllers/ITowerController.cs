﻿using System.Collections.Generic;
using TowerDefence.Interactor.ViewModels;
using UnityEngine;

namespace TowerDefence.Interactor.Controllers
{
    public interface ITowerController
    {
        IEnumerable<TowerPrefab> AllTowerPrefabs { get; }
        string BuyTower(string prefabId, Vector3 position, Quaternion rotation);
        void UpgradeTower(string towerId);
        TowerInfo GetTowerInfo(string towerId);
        TC AddComponentToTower<TC>(string towerId) where TC : Component;
    }
}