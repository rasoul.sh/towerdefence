﻿using JetBrains.Annotations;
using TowerDefence.Application.GameResources.Services;
using Zenject;

namespace TowerDefence.Interactor.Controllers
{
    [UsedImplicitly]
    public class GameResourceController : IGameResourceController
    {
        private IGameResourceService _gameResourceService;

        public int CurrentGolds => _gameResourceService.CurrentGolds;
        public int CurrentScore => _gameResourceService.CurrentScore;
        public event IGameResourceController.GoldDelegate OnGoldsAmountChanged;
        public event IGameResourceController.ScoreDelegate OnScoreAmountChanged;

        [Inject]
        public void Init(IGameResourceService gameResourceService)
        {
            _gameResourceService = gameResourceService;
            _gameResourceService.OnGoldAmountChanged += delegate(int amount) {  OnGoldsAmountChanged?.Invoke(amount); };
            _gameResourceService.OnScoreAmountChanged += delegate(int amount) { OnScoreAmountChanged?.Invoke(amount); };
        }
    }
}