﻿using System.Collections.Generic;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Interactor.Common.Mapper;
using TowerDefence.Interactor.Controllers;
using TowerDefence.Interactor.Mappers;
using TowerDefence.Interactor.ViewModels;
using Zenject;

namespace TowerDefence.Interactor
{
    public class InteractorInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            //mappers
            Container.Bind<IReadOnlyMapper<IEnumerable<LevelData>, LevelList>>().To<LevelListMapper>().FromNew().AsSingle();
            Container.Bind<IReadOnlyMapper<LevelData, LevelResult>>().To<LevelResultMapper>().FromNew().AsSingle();
            Container.Bind<IReadOnlyMapper<Tower, TowerPrefab>>().To<TowerPrefabMapper>().FromNew().AsSingle();
            Container.Bind<IReadOnlyMapper<Tower, TowerInfo>>().To<TowerInfoMapper>().FromNew().AsSingle();
            
            //controllers
            Container.Bind<ILevelController>().To<LevelController>().FromNew().AsSingle();
            Container.Bind<IGameResourceController>().To<GameResourceController>().FromNew().AsSingle();
            Container.Bind<ITowerController>().To<TowerController>().FromNew().AsSingle();
            Container.Bind<ISoldierController>().To<SoldierController>().FromNew().AsSingle();
        }
    }
}