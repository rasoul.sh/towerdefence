﻿namespace TowerDefence.Interactor.ViewModels
{
    public class TowerPrefab
    {
        public string PrefabId { get; set; }
        public string Title { get; set; }
        public int GoldCost { get; set; }
    }
}