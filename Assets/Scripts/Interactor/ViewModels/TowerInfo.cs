﻿namespace TowerDefence.Interactor.ViewModels
{
    public class TowerInfo
    {
        public string Title { get; set; }
        public int CurrentLevel { get; set; }
        public int CurrentRange { get; set; }
        public bool IsOnLastLevel { get; set; }
        public int UpgradeScoreNeeded { get; set; }
        public int UpgradeGoldCost { get; set; }
    }
}