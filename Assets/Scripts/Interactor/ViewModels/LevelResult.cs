﻿namespace TowerDefence.Interactor.ViewModels
{
    public class LevelResult
    {
        public bool Victory { get; set; }
        public int LostLives { get; set; }
    }
}