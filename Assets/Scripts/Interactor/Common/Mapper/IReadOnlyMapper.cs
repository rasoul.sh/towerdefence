﻿namespace TowerDefence.Interactor.Common.Mapper
{
    internal interface IReadOnlyMapper<TS,TT>
    {
        public TT MapToTarget(TS source);
    }
}