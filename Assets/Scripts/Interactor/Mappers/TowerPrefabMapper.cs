﻿using JetBrains.Annotations;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Interactor.Common.Mapper;
using TowerDefence.Interactor.ViewModels;

namespace TowerDefence.Interactor.Mappers
{
    [UsedImplicitly]
    public class TowerPrefabMapper : IReadOnlyMapper<Tower, TowerPrefab>
    {
        public TowerPrefab MapToTarget(Tower source)
        {
            return new TowerPrefab()
            {
                PrefabId = source.prefabId,
                Title = source.title,
                GoldCost = source.goldCost
            };
        }
    }
}