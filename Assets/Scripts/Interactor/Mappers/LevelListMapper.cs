﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Interactor.Common.Mapper;
using TowerDefence.Interactor.ViewModels;

namespace TowerDefence.Interactor.Mappers
{
    [UsedImplicitly]
    public class LevelListMapper : IReadOnlyMapper<IEnumerable<LevelData>,LevelList>
    {
        public LevelList MapToTarget(IEnumerable<LevelData> source)
        {
            var result = new List<int>();
            var levelDatas = source.ToList();
            for (int i = 0; i < levelDatas.Count; i++)
            {
                result.Add(i + 1);
            }
            return new LevelList(){ Levels = result.ToArray()};
        }
    }
}