﻿using JetBrains.Annotations;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Enterprise.Enums;
using TowerDefence.Interactor.Common.Mapper;
using TowerDefence.Interactor.ViewModels;

namespace TowerDefence.Interactor.Mappers
{
    [UsedImplicitly]
    public class LevelResultMapper : IReadOnlyMapper<LevelData, LevelResult>
    {
        public LevelResult MapToTarget(LevelData source)
        {
            return new LevelResult()
            {
                Victory = source.currentLevelState == LevelState.Victory,
                LostLives = source.maxLives - source.currentLives
            };
        }
    }
}