﻿using JetBrains.Annotations;
using TowerDefence.Enterprise.Entities;
using TowerDefence.Interactor.Common.Mapper;
using TowerDefence.Interactor.ViewModels;

namespace TowerDefence.Interactor.Mappers
{
    [UsedImplicitly]
    public class TowerInfoMapper : IReadOnlyMapper<Tower, TowerInfo>
    {
        public TowerInfo MapToTarget(Tower source)
        {
            var isOnLastLevel = source.IsLastUpgrade;
            return new TowerInfo()
            {
                Title = source.title,
                IsOnLastLevel = isOnLastLevel,
                CurrentLevel = source.currentLevelIndex + 1,
                CurrentRange = source.CurrentLevel.maxRange,
                UpgradeScoreNeeded = isOnLastLevel ? 0 : source.levels[source.currentLevelIndex + 1].scoreNeeded,
                UpgradeGoldCost = isOnLastLevel ? 0 : source.levels[source.currentLevelIndex + 1].upgradeCost
            };
        }
    }
}