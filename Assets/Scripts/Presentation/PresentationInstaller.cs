﻿using TowerDefence.Presentation.Panels;
using TowerDefence.Presentation.Views;
using TowerDefence.Presentation.Views.InGameViews;
using TowerDefence.Presentation.Views.MenuViews;
using UnityEngine;
using Zenject;

namespace TowerDefence.Presentation
{
    public class PresentationInstaller : MonoInstaller
    {
        [SerializeField] private MenuPanel menuPanel;
        [SerializeField] private InGamePanel inGamePanel;
        [SerializeField] private MainMenuView mainMenuView;
        [SerializeField] private SelectLevelView selectLevelView;
        [SerializeField] private BuildingPointView buildingPointView;
        [SerializeField] private TowerView towerView;
        [SerializeField] private PauseMenuView pauseMenuView;
        
        public override void InstallBindings()
        {
            //panels
            Container.Bind<IMenuPanel>().To<MenuPanel>().FromInstance(menuPanel).AsSingle();
            Container.Bind<IInGamePanel>().To<InGamePanel>().FromInstance(inGamePanel).AsSingle();
            
            //views
            Container.Bind<IMainMenuView>().To<MainMenuView>().FromInstance(mainMenuView).AsSingle();
            Container.Bind<ISelectLevelView>().To<SelectLevelView>().FromInstance(selectLevelView).AsSingle();
            Container.Bind<IBuildingPointView>().To<BuildingPointView>().FromInstance(buildingPointView).AsSingle();
            Container.Bind<ITowerView>().To<TowerView>().FromInstance(towerView).AsSingle();
            Container.Bind<IPauseMenuView>().To<PauseMenuView>().FromInstance(pauseMenuView).AsSingle();
        }
    }
}