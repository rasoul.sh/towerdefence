﻿using TweenerSystem;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefence.Presentation.Common.ButtonComponents
{
    [RequireComponent(typeof(Button))]
    public class SelectableButton : MonoBehaviour
    {
        [SerializeField] private Tweener selectTweener;
        private Button _button;
        public Button Button => _button ??= GetComponent<Button>();

        public void Select(bool ignoreAnimate = false) => ToggleSelect(true, ignoreAnimate);

        public void Deselect(bool ignoreAnimate = false) => ToggleSelect(false, ignoreAnimate);

        public void ToggleSelect(bool state, bool ignoreAnimate = false) => selectTweener.Play(state, ignoreAnimate);
    }
}