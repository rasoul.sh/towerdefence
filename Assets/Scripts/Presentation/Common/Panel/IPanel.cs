﻿namespace TowerDefence.Presentation.Common.Panel
{
    public interface IPanel
    {
        void Show(bool ignoreAnimate = false);
        void Hide(bool ignoreAnimate = false);
        bool IsShown { get; }
    }
}