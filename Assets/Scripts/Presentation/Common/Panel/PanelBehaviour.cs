﻿using UIToolkit.GUIPanelSystem;
using UnityEngine;

namespace TowerDefence.Presentation.Common.Panel
{
    [RequireComponent(typeof(GUIPanel))]
    public class PanelBehaviour : MonoBehaviour, IPanel
    {
        private GUIPanel _guiPanel;
        private GUIPanel GuiPanel => _guiPanel ??= GetComponent<GUIPanel>();
        
        public void Show(bool ignoreAnimate = false)
        {
            GuiPanel.Toggle(true, ignoreAnimate);
        }

        public virtual void Hide(bool ignoreAnimate = false)
        {
            GuiPanel.Toggle(false, ignoreAnimate);
        }

        public bool IsShown => GuiPanel.IsShown;
    }
}