﻿using UIToolkit.Billboard;
using UIToolkit.InteractionHelpers;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TowerDefence.Presentation.Common.Panel
{
    [RequireComponent(typeof(SimpleBillboard))]
    public class BillboardPanelBehaviour : PanelBehaviour,
        IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private PointerInputBehaviour hideInput;
        protected Transform billboardTarget;
        private SimpleBillboard _billboard;
        private SimpleBillboard Billboard => _billboard ??= GetComponent<SimpleBillboard>();
        private bool _isPointerOver;
        private float _lastShownTime = 0f;
        private const float _minShownTime = 0.1f;

        protected virtual void Start()
        {
            hideInput.OnClick += OnHideInputClicked;
        }

        private void OnHideInputClicked(PointerInputBehaviour asset)
        {
            if (_isPointerOver)
            {
                return;
            }
            if (Time.realtimeSinceStartup - _lastShownTime < _minShownTime)
            {
                return;
            }
            Hide();
        }

        public void Show(Transform billboardTarget)
        {
            this.billboardTarget = billboardTarget;
            Billboard.Target = this.billboardTarget;
            Show();
            _lastShownTime = Time.realtimeSinceStartup;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _isPointerOver = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _isPointerOver = false;
        }
    }
}