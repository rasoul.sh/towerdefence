﻿using TowerDefence.Presentation.Common.Panel;
using UnityEngine;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public interface ITowerView : IPanel
    {
        void Show(string towerId, Transform tower);
    }
}