﻿using System.Collections.Generic;
using TowerDefence.Interactor.Controllers;
using TowerDefence.Presentation.Common.Panel;
using TowerDefence.Presentation.UserInteraction;
using TowerDefence.Presentation.Views.InGameViews.SubViews;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public class BuildingPointView : BillboardPanelBehaviour, IBuildingPointView
    {
        [Inject] private ITowerController _towerController;
        [Inject] private IGameResourceController _gameResourceController;
        [SerializeField] private GridLayoutGroup buildingGrid;
        [SerializeField] private TowerPrefabSubView towerPrefabItem;
        private List<TowerPrefabSubView> _currentItems;

        protected override void Start()
        {
            base.Start();
            _currentItems = new();
            foreach (var towerPrefab in _towerController.AllTowerPrefabs)
            {
                var newItem = Instantiate(towerPrefabItem, buildingGrid.transform);
                newItem.UpdateGUI(towerPrefab, _gameResourceController.CurrentGolds);
                newItem.OnSelect += OnSelectItem;
                _currentItems.Add(newItem);
            }
            _gameResourceController.OnGoldsAmountChanged += OnGoldsAmountChanged;
        }

        private void OnSelectItem(string prefabId)
        {
            Hide();
            var towerId = _towerController.BuyTower(prefabId, billboardTarget.position, billboardTarget.rotation);
            var selectableTower = _towerController.AddComponentToTower<SelectableTower>(towerId);
            selectableTower.TowerId = towerId;
            billboardTarget.gameObject.SetActive(false);
        }

        private void OnGoldsAmountChanged(int golds)
        {
            _currentItems.ForEach(item => item.UpdateGUI(golds));
        }
    }
}