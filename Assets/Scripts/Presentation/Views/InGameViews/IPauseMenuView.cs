﻿using TowerDefence.Presentation.Common.Panel;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public interface IPauseMenuView : IPanel
    {
    }
}