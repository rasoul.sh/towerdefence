﻿using TowerDefence.Interactor.Controllers;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public class PauseView : MonoBehaviour
    {
        [Inject] private ILevelController _levelController;
        [Inject] private IPauseMenuView _pauseMenuView;
        [SerializeField] private Button pauseButton;

        private void Start()
        {
            pauseButton.onClick.AddListener(Pause);
        }

        private void Pause()
        {
            _pauseMenuView.Show();
            _levelController.PauseGame();
        }
    }
}