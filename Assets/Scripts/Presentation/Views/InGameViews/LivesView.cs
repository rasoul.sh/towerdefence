﻿using TowerDefence.Interactor.Controllers;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public class LivesView : MonoBehaviour
    {
        [Inject] private ILevelController _levelController;
        [SerializeField] private Text livesCountLabel;

        private void OnEnable()
        {
            UpdateLivesLabel(_levelController.CurrentLives);
        }

        private void Start()
        {
            _levelController.OnLivesChanged += UpdateLivesLabel;
        }

        private void UpdateLivesLabel(int lives)
        {
            livesCountLabel.text = lives.ToString();
        }
    }
}