﻿using TowerDefence.Interactor.Controllers;
using TowerDefence.Presentation.Common.Panel;
using TowerDefence.Presentation.Panels;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public class PauseMenuView : PanelBehaviour, IPauseMenuView
    {
        [Inject] private ILevelController _levelController;
        [Inject] private IMenuPanel _menuPanel;
        [Inject] private IInGamePanel _inGamePanel;
        [SerializeField] private Button resumeButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button mainMenuButton;
        [SerializeField] private Button quitGameButton;
        private void Start()
        {
            resumeButton.onClick.AddListener(Resume);
            restartButton.onClick.AddListener(Restart);
            mainMenuButton.onClick.AddListener(GoToMainMenu);
            quitGameButton.onClick.AddListener(_levelController.QuitGame);
        }

        private void GoToMainMenu()
        {
            _levelController.ResumeGame();
            _levelController.QuitLevel();
            _menuPanel.ShowMainMenuView();
            Hide(true);
            _inGamePanel.Hide();
        }

        private void Restart()
        {
            _levelController.ResumeGame();
            _levelController.RestartLevel();
            Hide(true);
        }

        private void Resume()
        {
            _levelController.ResumeGame();
            Hide();
        }
    }
}