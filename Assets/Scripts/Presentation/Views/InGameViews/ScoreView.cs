﻿using TowerDefence.Interactor.Controllers;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public class ScoreView : MonoBehaviour
    {
        [Inject] private IGameResourceController _gameResourceController;
        [SerializeField] private Text scoreCountLabel;

        private void OnEnable()
        {
            UpdateLabel(_gameResourceController.CurrentScore);
        }

        private void Start()
        {
            _gameResourceController.OnScoreAmountChanged += UpdateLabel;
        }

        private void UpdateLabel(int score)
        {
            scoreCountLabel.text = score.ToString();
        }
    }
}