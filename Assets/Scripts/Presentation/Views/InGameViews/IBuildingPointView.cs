﻿using TowerDefence.Presentation.Common.Panel;
using UnityEngine;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public interface IBuildingPointView : IPanel
    {
        void Show(Transform buildingPoint);
    }
}