﻿using TowerDefence.Interactor.ViewModels;
using TweenerSystem;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefence.Presentation.Views.InGameViews.SubViews
{
    public class TowerPrefabSubView : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Text titleLabel;
        [SerializeField] private Text goldCostLabel;
        [SerializeField] private Tweener insufficientGoldTweener;
        private TowerPrefab _data;
        public event SelectDelegate OnSelect;
        public delegate void SelectDelegate(string prefabId);

        private void Start()
        {
            button.onClick.AddListener(Select);
        }

        private void Select()
        {
            OnSelect?.Invoke(_data.PrefabId);
        }

        public void UpdateGUI(TowerPrefab data, int currentGolds)
        {
            _data = data;
            UpdateGUI(currentGolds);
        }

        public void UpdateGUI(int currentGolds)
        {
            var isGoldSufficient = _data.GoldCost <= currentGolds;
            button.interactable = isGoldSufficient;
            titleLabel.text = _data.Title;
            goldCostLabel.text = _data.GoldCost.ToString();
            insufficientGoldTweener.Play(isGoldSufficient == false);
        }
    }
}