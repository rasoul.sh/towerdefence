﻿using TweenerSystem;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefence.Presentation.Views.InGameViews.SubViews
{
    public class UpgradeButtonSubView : MonoBehaviour
    {
        [SerializeField] private Text goldCostLabel;
        [SerializeField] private Text scoreNeededLabel;
        [SerializeField] private Tweener goldInsufficientTweener;
        [SerializeField] private Tweener scoreInsufficientTweener;
        [SerializeField] private Button button;
        public event SelectDelegate OnSelect;

        public delegate void SelectDelegate();

        private void Start()
        {
            button.onClick.AddListener(Select);
        }

        private void Select()
        {
            OnSelect?.Invoke();
        }

        public void UpdateGUI(int goldCost, int scoreNeeded, int currentGolds,
            int currentScore)
        {
            var insufficientGold = currentGolds < goldCost;
            var insufficientScore = currentScore < scoreNeeded;
            goldCostLabel.text = goldCost.ToString();
            scoreNeededLabel.text = scoreNeeded.ToString();
            goldInsufficientTweener.Play(insufficientGold, true);
            scoreInsufficientTweener.Play(insufficientScore, true);
            button.interactable = insufficientGold == false && insufficientScore == false;
        }
    }
}