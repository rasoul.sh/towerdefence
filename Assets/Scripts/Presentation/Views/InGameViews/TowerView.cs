﻿using TowerDefence.Interactor.Controllers;
using TowerDefence.Interactor.ViewModels;
using TowerDefence.Presentation.Common.Panel;
using TowerDefence.Presentation.Views.InGameViews.SubViews;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public class TowerView : BillboardPanelBehaviour, ITowerView
    {
        [Inject] private ITowerController _towerController;
        [Inject] private IGameResourceController _gameResourceController;
        [SerializeField] private Text titleLabel;
        [SerializeField] private Text levelLabel;
        [SerializeField] private Text rangeLabel;
        [SerializeField] private GameObject maxUpgradeGraphic;
        [SerializeField] private UpgradeButtonSubView upgradeButton;

        private string _towerId;
        private TowerInfo _towerInfo;

        protected override void Start()
        {
            base.Start();
            upgradeButton.OnSelect += Upgrade;
            _gameResourceController.OnGoldsAmountChanged += delegate { UpdateUpgradeGUI();};
            _gameResourceController.OnScoreAmountChanged += delegate { UpdateUpgradeGUI();};
        }

        private void Upgrade()
        {
            _towerController.UpgradeTower(_towerId);
            _towerInfo = _towerController.GetTowerInfo(_towerId);
            UpdateLevelGUI();
            UpdateRangeGUI();
            UpdateUpgradeGUI();
        }

        public void Show(string towerId, Transform tower)
        {
            if (_towerId == towerId)
            {
                return;
            }
            _towerId = towerId;
            _towerInfo = _towerController.GetTowerInfo(towerId);
            titleLabel.text = _towerInfo.Title;
            UpdateLevelGUI();
            UpdateRangeGUI();
            UpdateUpgradeGUI();
            Show(tower);
        }

        public override void Hide(bool ignoreAnimate = false)
        {
            _towerId = null;
            _towerInfo = null;
            base.Hide(ignoreAnimate);
        }

        private void UpdateLevelGUI()
        {
            levelLabel.text = "Level " + _towerInfo.CurrentLevel;
        }
        
        private void UpdateRangeGUI()
        {
            rangeLabel.text = "Range: " + _towerInfo.CurrentRange;
        }

        private void UpdateUpgradeGUI()
        {
            if (string.IsNullOrEmpty(_towerId))
            {
                return;
            }
            maxUpgradeGraphic.SetActive(_towerInfo.IsOnLastLevel);
            upgradeButton.gameObject.SetActive(_towerInfo.IsOnLastLevel == false);
            if (_towerInfo.IsOnLastLevel)
            {
                return;
            }
            var golds = _gameResourceController.CurrentGolds;
            var score = _gameResourceController.CurrentScore;
            upgradeButton.UpdateGUI(_towerInfo.UpgradeGoldCost, _towerInfo.UpgradeScoreNeeded,
                golds, score);
        }
    }
}