﻿using TowerDefence.Enterprise.Enums;
using TowerDefence.Interactor.Controllers;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public class WaveView : MonoBehaviour
    {
        [Inject] private ILevelController _levelController;
        [SerializeField] private GameObject upcomingGraphic;
        [SerializeField] private Text waveCountText;

        private void Start()
        {
            upcomingGraphic.SetActive(false);
            waveCountText.gameObject.SetActive(false);
            _levelController.OnQuit += OnLevelQuited;
            _levelController.OnWaveComing += OnWaveComing;
            _levelController.OnWaveStarted += OnWaveStarted;
        }

        private void OnWaveStarted(int waveNumber, bool isFinalWave)
        {
            upcomingGraphic.SetActive(false);
            waveCountText.text = isFinalWave ? "Final Wave" : "Wave " + waveNumber;
            waveCountText.gameObject.SetActive(true);
        }

        private void OnWaveComing(int waveNumber)
        {
            upcomingGraphic.SetActive(true);
        }
        
        

        private void OnLevelQuited(QuitLevelReason quitLevelReason)
        {
            upcomingGraphic.SetActive(false);
            waveCountText.gameObject.SetActive(false);
        }
    }
}