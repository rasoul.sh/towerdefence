﻿using System;
using TowerDefence.Enterprise.Enums;
using TowerDefence.Interactor.Controllers;
using TowerDefence.Presentation.Common.ButtonComponents;
using UnityEngine;
using Zenject;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public class GameSpeedView : MonoBehaviour
    {
        [Inject] private ILevelController _levelController;
        [SerializeField] private SelectableButton forwardButton;

        private void OnEnable()
        {
            forwardButton.ToggleSelect(_levelController.CurrentGameSpeed == GameSpeeds.DoubleSpeed, true);
        }

        private void Start()
        {
            forwardButton.Button.onClick.AddListener(ToggleGameSpeed);
            _levelController.OnGameSpeedChanged += OnGameSpeedChanged;
        }

        private void OnGameSpeedChanged(GameSpeeds gameSpeed)
        {
            forwardButton.ToggleSelect(gameSpeed == GameSpeeds.DoubleSpeed, true);
        }

        private void ToggleGameSpeed()
        {
            var targetGameSpeed = _levelController.CurrentGameSpeed == GameSpeeds.NormalSpeed
                ? GameSpeeds.DoubleSpeed
                : GameSpeeds.NormalSpeed;
            _levelController.SetGameSpeed(targetGameSpeed);
        }
    }
}