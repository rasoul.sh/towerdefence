﻿using TowerDefence.Interactor.Controllers;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TowerDefence.Presentation.Views.InGameViews
{
    public class GoldView : MonoBehaviour
    {
        [Inject] private IGameResourceController _gameResourceController;
        [SerializeField] private Text goldCountLabel;

        private void OnEnable()
        {
            UpdateLabel(_gameResourceController.CurrentGolds);
        }

        private void Start()
        {
            _gameResourceController.OnGoldsAmountChanged += UpdateLabel;
        }

        private void UpdateLabel(int golds)
        {
            goldCountLabel.text = golds.ToString();
        }
    }
}