using TowerDefence.Interactor.Controllers;
using TowerDefence.Presentation.Common.Panel;
using TowerDefence.Presentation.Panels;
using TowerDefence.Presentation.Views.MenuViews.Subviews;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TowerDefence.Presentation.Views.MenuViews
{
    public class SelectLevelView : PanelBehaviour, ISelectLevelView
    {
        [Inject] private ILevelController _levelController;
        [Inject] private IMenuPanel _menuPanel;
        [Inject] private IInGamePanel _inGamePanel;

        [SerializeField] private GridLayoutGroup levelGrid;
        [SerializeField] private LevelSubView levelItemPrefab;

        private void Start()
        {
            foreach (var levelNumber in _levelController.AllLevels.Levels)
            {
                var newItem = levelItemPrefab.Instantiate(levelNumber, SelectLevel);
                newItem.transform.SetParent(levelGrid.transform);
            }
        }

        private void SelectLevel(int levelNumber)
        {
            _levelController.SelectLevel(levelNumber);
            _menuPanel.Hide();
            _inGamePanel.Show();
        }
    }
}
