﻿using UnityEngine;
using UnityEngine.UI;

namespace TowerDefence.Presentation.Views.MenuViews.Subviews
{
    public class LevelSubView : MonoBehaviour
    {
        [SerializeField] private Text levelNumberLabel;
        [SerializeField] private Button button;
        private SelectDelegate _onSelect;
        public delegate void SelectDelegate(int levelNumber);
        
        private int _levelNumber;

        private void Start()
        {
            button.onClick.AddListener(Select);
        }

        private void Select()
        {
            _onSelect?.Invoke(_levelNumber);
        }

        public LevelSubView Instantiate(int levelNumber, SelectDelegate onSelect)
        {
            var instance = Instantiate(this);
            instance._onSelect = onSelect;
            instance._levelNumber = levelNumber;
            instance.levelNumberLabel.text = levelNumber.ToString();
            return instance;
        }
    }
}