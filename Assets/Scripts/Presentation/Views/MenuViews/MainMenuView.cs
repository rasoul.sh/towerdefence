using TowerDefence.Interactor.Controllers;
using TowerDefence.Presentation.Common;
using TowerDefence.Presentation.Common.Panel;
using TowerDefence.Presentation.Panels;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TowerDefence.Presentation.Views.MenuViews
{
    public class MainMenuView : PanelBehaviour, IMainMenuView
    {
        [Inject] private ILevelController _levelController;
        [Inject] private IMenuPanel _menuPanel;
        [Inject] private ISelectLevelView _selectLevelView;
        
        [SerializeField] private Button startGameButton;
        [SerializeField] private Button quitGameButton;

        private void Start()
        {
            startGameButton.onClick.AddListener(_menuPanel.ShowSelectLevelView);
            quitGameButton.onClick.AddListener(_levelController.QuitGame);
        }
    }
}
