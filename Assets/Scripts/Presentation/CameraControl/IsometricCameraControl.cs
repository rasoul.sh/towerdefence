﻿using System;
using UnityEngine;

namespace TowerDefence.Presentation.CameraControl
{
    [RequireComponent(typeof(Camera))]
    public class IsometricCameraControl : MonoBehaviour
    {
        [SerializeField] private float defaultSize = 30f;
        [SerializeField] private float minSize = 15f;
        [SerializeField] private float maxSize = 30f;
        [SerializeField] private Vector2 xRange;
        [SerializeField] private Vector2 zRange;
        [SerializeField] private float panElasticity = 10f;
        [SerializeField] private float zoomElasticity = 4f;
        [SerializeField] private float elasticityVelocity = 20f;
        [SerializeField] private float panVelocity = 2f;
        [SerializeField] private float zoomVelocity = 4f;
        private Camera _camera;
        private Transform _transform;
        private const float ZoomMultiplier = 200f;


        private void Start()
        {
            _camera = GetComponent<Camera>();
            _transform = transform;
            _camera.orthographicSize = defaultSize;
        }

        private void Update()
        {
            _transform.localPosition = Vector3.MoveTowards(
                _transform.localPosition, GetClampedPosition(0),
                elasticityVelocity * Time.deltaTime);
            var orthographicSize = _camera.orthographicSize;
            _camera.orthographicSize = Mathf.MoveTowards(
                orthographicSize, Mathf.Clamp(orthographicSize,
                    minSize, maxSize), elasticityVelocity * Time.deltaTime);
        }

        private void LateUpdate()
        {
            _transform.localPosition = GetClampedPosition(panElasticity);
            _camera.orthographicSize = Mathf.Clamp(_camera.orthographicSize,
                minSize - zoomElasticity, maxSize + zoomElasticity);
        }

        public void Pan(Vector2 direction)
        {
            var pos = _transform.localPosition;
            var orthographicSize = _camera.orthographicSize;
            pos.x += -direction.y * Time.deltaTime * panVelocity / orthographicSize;
            pos.z += direction.x * Time.deltaTime * panVelocity / orthographicSize;
            _transform.localPosition = pos;
        }

        public void Zoom(float zoom)
        {
            _camera.orthographicSize += zoom * Time.deltaTime * zoomVelocity * ZoomMultiplier;
        }

        private Vector3 GetClampedPosition(float elasticityValue)
        {
            var pos = _transform.localPosition;
            pos.x = Mathf.Clamp(pos.x, xRange.x - elasticityValue,
                xRange.y + elasticityValue);
            pos.z = Mathf.Clamp(pos.z, zRange.x - elasticityValue,
                zRange.y + elasticityValue);
            return pos;
        }
    }
}