﻿using System;
using UIToolkit.InteractionHelpers;
using UnityEngine;

namespace TowerDefence.Presentation.CameraControl
{
    [RequireComponent(typeof(IsometricCameraControl))]
    public class IsometricCameraInput : MonoBehaviour
    {
        [SerializeField] private PointerInputBehaviour[] pointerInputs;
        private IsometricCameraControl _cameraControl;

        private void Start()
        {
            _cameraControl = GetComponent<IsometricCameraControl>();
            foreach (var pointerInput in pointerInputs)
            {
                pointerInput.OnDragging += OnDragging;
            }
        }

        private void Update()
        {
            var scroll = Input.GetAxis("Mouse ScrollWheel");
            if (scroll != 0)
            {
                _cameraControl.Zoom(-scroll);
            }
        }

        private void OnDragging(PointerInputBehaviour asset)
        {
            _cameraControl.Pan(asset.CurrentDragDirection);
        }
    }
}