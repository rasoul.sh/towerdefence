﻿using TowerDefence.Presentation.Views.InGameViews;
using UIToolkit.InteractionHelpers;
using Zenject;

namespace TowerDefence.Presentation.UserInteraction
{
    public class SelectableTower : ColliderInputBehaviour
    {
        [Inject] private ITowerView _towerView;
        public string TowerId { get; set; }

        protected override void OnClickAction()
        {
            _towerView.Show(TowerId, transform);
        }
    }
}