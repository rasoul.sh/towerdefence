﻿using System;
using TowerDefence.Enterprise.Enums;
using TowerDefence.Interactor.Controllers;
using TowerDefence.Presentation.Views.InGameViews;
using UIToolkit.InteractionHelpers;
using Zenject;

namespace TowerDefence.Presentation.UserInteraction
{
    public class BuildingPoint : ColliderInputBehaviour
    {
        [Inject] private ILevelController _levelController;
        [Inject] private IBuildingPointView _buildingPointView;

        private void Start()
        {
            _levelController.OnQuit += OnLevelQuited;
        }

        private void OnLevelQuited(QuitLevelReason quitLevelReason)
        {
            gameObject.SetActive(true);
        }

        protected override void OnClickAction()
        {
            _buildingPointView.Show(transform);
        }
    }
}