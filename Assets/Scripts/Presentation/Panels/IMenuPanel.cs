﻿using TowerDefence.Presentation.Common.Panel;

namespace TowerDefence.Presentation.Panels
{
    public interface IMenuPanel : IPanel
    {
        void ShowMainMenuView();
        void ShowSelectLevelView();
    }
}