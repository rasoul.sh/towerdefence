﻿using TowerDefence.Presentation.Common.Panel;
using TowerDefence.Presentation.Views.MenuViews;
using Zenject;

namespace TowerDefence.Presentation.Panels
{
    public class MenuPanel : PanelBehaviour, IMenuPanel
    {
        [Inject] private IMainMenuView _mainMenuView;
        [Inject] private ISelectLevelView _selectLevelView;
        private IPanel _currentView;

        private void Start()
        {
            _currentView = _mainMenuView;
        }

        public void ShowMainMenuView() => ChangeView(_mainMenuView);

        public void ShowSelectLevelView() => ChangeView(_selectLevelView);

        private void ChangeView(IPanel view)
        {
            var ignoreAnimate = IsShown == false;
            _currentView.Hide(ignoreAnimate);
            view.Show(ignoreAnimate);
            _currentView = view;
            Show();
        }
    }
}