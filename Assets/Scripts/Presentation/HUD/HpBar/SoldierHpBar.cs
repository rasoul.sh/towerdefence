﻿using System;
using UIToolkit.Billboard;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefence.Presentation.HUD.HpBar
{
    [RequireComponent(typeof(SimpleBillboard))]
    [RequireComponent(typeof(CanvasGroup))]
    public class SoldierHpBar : MonoBehaviour
    {
        [SerializeField] private Color fullColor;
        [SerializeField] private Color emptyColor;
        [SerializeField] private Slider hpSlider;
        [SerializeField] private Graphic fillGraphic;
        public string SoldierId { get; private set; }
        private CanvasGroup _canvasGroup;

        private void Start()
        {
            _canvasGroup.alpha = 1f;
        }

        public SoldierHpBar Instantiate(Transform panel, Transform soldier, string soldierId)
        {
            var instance = Instantiate(this, panel);
            instance._canvasGroup = GetComponent<CanvasGroup>();
            instance._canvasGroup.alpha = 0f;
            instance.GetComponent<SimpleBillboard>().Target = soldier;
            instance.SoldierId = soldierId;
            instance.hpSlider.value = 1f;
            return instance;
        }

        public void UpdateGUI(float hpValue)
        {
            hpSlider.value = hpValue;
            fillGraphic.color = Color.Lerp(emptyColor, fullColor, hpValue);
        }
    }
}