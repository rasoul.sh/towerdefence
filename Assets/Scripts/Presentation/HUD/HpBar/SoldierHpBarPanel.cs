﻿using System.Collections.Generic;
using TowerDefence.Interactor.Controllers;
using UnityEngine;
using Zenject;

namespace TowerDefence.Presentation.HUD.HpBar
{
    public class SoldierHpBarPanel : MonoBehaviour
    {
        [Inject] private ISoldierController _soldierController;
        [SerializeField] private SoldierHpBar hpBarPrefab;
        private Dictionary<string, SoldierHpBar> _hpBarDict;
        private Transform _transform;
        private Transform Transform => _transform ??= GetComponent<Transform>();

        private void Start()
        {
            _hpBarDict = new Dictionary<string, SoldierHpBar>();
            _soldierController.OnSoldierSpawned += AddHpBar;
            _soldierController.OnSoldierDestroyed += OnSoldierDestroyed;
            _soldierController.OnHpValueChanged += OnHpValueChanged;
        }

        private void OnHpValueChanged(string soldierId, float value)
        {
            _hpBarDict[soldierId].UpdateGUI(value);
        }

        private void OnSoldierDestroyed(string soldierId)
        {
            Destroy(_hpBarDict[soldierId].gameObject);
            _hpBarDict.Remove(soldierId);
        }

        private void AddHpBar(Transform soldier, string soldierId)
        {
            var hpBarInstance = hpBarPrefab.Instantiate(Transform, soldier, soldierId);
            _hpBarDict.Add(soldierId, hpBarInstance);
        }
    }
}